--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5
-- Dumped by pg_dump version 10.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: arrows; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.arrows (
    id integer NOT NULL,
    compendium_id integer,
    compendium_id_dlc_2 integer,
    compendium_id_master_mode integer,
    compendium_id_master_mode_dlc_2 integer,
    name text NOT NULL,
    attack_power_min integer,
    attack_power_max integer,
    effects text,
    description text
);


ALTER TABLE public.arrows OWNER TO webserver;

--
-- Name: bows; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.bows (
    id integer NOT NULL,
    compendium_id integer,
    compendium_id_dlc_2 integer,
    compendium_id_master_mode integer,
    compendium_id_master_mode_dlc_2 integer,
    name text NOT NULL,
    attack_power integer,
    durability integer,
    range integer,
    fire_rate integer,
    locations integer[],
    dropped_by text,
    description text
);


ALTER TABLE public.bows OWNER TO webserver;

--
-- Name: creatures; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.creatures (
    id integer NOT NULL,
    compendium_id integer,
    compendium_id_dlc_2 integer,
    compendium_id_master_mode integer,
    compendium_id_master_mode_dlc_2 integer,
    name text NOT NULL,
    creature_type text NOT NULL,
    recoverable_materials integer[],
    description text NOT NULL
);


ALTER TABLE public.creatures OWNER TO webserver;

--
-- Name: location_types; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.location_types (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.location_types OWNER TO webserver;

--
-- Name: locations; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.locations (
    id integer NOT NULL,
    name text NOT NULL,
    region_fk integer,
    subregion_fk integer,
    location_type_fk integer
);


ALTER TABLE public.locations OWNER TO webserver;

--
-- Name: regions; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.regions (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.regions OWNER TO webserver;

--
-- Name: subregions; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.subregions (
    id integer NOT NULL,
    name text NOT NULL,
    region_fk integer
);


ALTER TABLE public.subregions OWNER TO webserver;

--
-- Name: location_views; Type: MATERIALIZED VIEW; Schema: public; Owner: webserver
--

CREATE MATERIALIZED VIEW public.location_views AS
 SELECT r.id AS region_id,
    r.name AS region,
    s.id AS subregion_id,
    s.name AS subregion,
    l.id AS location_id,
    l.name AS location,
    t.id AS location_type_id,
    t.name AS location_type
   FROM (((public.locations l
     LEFT JOIN public.regions r ON ((r.id = l.region_fk)))
     LEFT JOIN public.subregions s ON ((s.id = l.subregion_fk)))
     LEFT JOIN public.location_types t ON ((t.id = l.location_type_fk)))
  ORDER BY l.id
  WITH NO DATA;


ALTER TABLE public.location_views OWNER TO webserver;

--
-- Name: materials; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.materials (
    id integer NOT NULL,
    compendium_id integer NOT NULL,
    compendium_id_dlc_2 integer NOT NULL,
    compendium_id_master_mode integer NOT NULL,
    compendium_id_master_mode_dlc_2 integer NOT NULL,
    name text NOT NULL,
    material_type text NOT NULL,
    value integer NOT NULL,
    restores numeric,
    additional_uses integer[],
    description text
);


ALTER TABLE public.materials OWNER TO webserver;

--
-- Name: materials_additional_uses; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.materials_additional_uses (
    id integer NOT NULL,
    additional_use text NOT NULL
);


ALTER TABLE public.materials_additional_uses OWNER TO webserver;

--
-- Name: monsters; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.monsters (
    id integer NOT NULL,
    compendium_id integer,
    compendium_id_dlc_2 integer,
    compendium_id_master_mode integer,
    compendium_id_master_mode_dlc_2 integer,
    name text NOT NULL,
    monster_type text NOT NULL,
    health integer,
    base_damage integer,
    recoverable_materials integer[],
    description text NOT NULL
);


ALTER TABLE public.monsters OWNER TO webserver;

--
-- Name: recoverable_materials; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.recoverable_materials (
    id integer NOT NULL,
    compendium_id integer,
    compendium_id_dlc_2 integer,
    compendium_id_master_mode integer,
    compendium_id_master_mode_dlc_2 integer,
    name text NOT NULL,
    material_type text NOT NULL,
    value integer,
    restores numeric,
    additional_uses integer[],
    description text
);


ALTER TABLE public.recoverable_materials OWNER TO webserver;

--
-- Name: shields; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.shields (
    id integer NOT NULL,
    compendium_id integer,
    compendium_id_dlc_2 integer,
    compendium_id_master_mode integer,
    compendium_id_master_mode_dlc_2 integer,
    name text NOT NULL,
    strength integer,
    durability integer,
    locations integer[],
    dropped_by text,
    description text
);


ALTER TABLE public.shields OWNER TO webserver;

--
-- Name: treasures; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.treasures (
    id integer NOT NULL,
    compendium_id integer NOT NULL,
    compendium_id_dlc_2 integer NOT NULL,
    compendium_id_master_mode integer NOT NULL,
    compendium_id_master_mode_dlc_2 integer NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    recoverable_materials integer[]
);


ALTER TABLE public.treasures OWNER TO webserver;

--
-- Name: weapons; Type: TABLE; Schema: public; Owner: webserver
--

CREATE TABLE public.weapons (
    id integer NOT NULL,
    compendium_id integer,
    compendium_id_dlc_2 integer,
    compendium_id_master_mode integer,
    compendium_id_master_mode_dlc_2 integer,
    name text NOT NULL,
    hands integer,
    weapon_type text,
    attack_power integer,
    durability integer,
    locations integer[],
    dropped_by text,
    description text
);


ALTER TABLE public.weapons OWNER TO webserver;

--
-- Data for Name: arrows; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.arrows (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, attack_power_min, attack_power_max, effects, description) FROM stdin;
1	344	348	349	353	Arrow	0	0	None	A common arrow. Its shaft was carved from the wood of a sturdy tree.
2	345	349	350	354	Fire Arrow	0	10	Elemental / Ignite: 40-60 ignite damage over time	An arrow imbued with the power of fire. It breaks apart on impact, igniting objects in the immediate area. It's incredibly effective against cold things.
3	346	350	351	355	Ice Arrow	0	10	Elemental / Cold: 40-60 damage if the ice locked target gets hit again.	An arrow imbued with the power of ice. It breaks apart on impact, freezing objects in the immediate area. It's incredibly effective against hot things.
4	347	351	352	356	Shock Arrow	0	20	Elemental / Electricity	An arrow imbued with the power of electricity. It breaks apart on impact, channeling electricity into nearby objects. It's incredibly effective against metal or anything wet.
5	348	352	353	357	Bomb Arrow	40	55	Special / Explode: can ignite targets.	A powerful arrow designed to destroy monsters. The explosive powder packed into the tip ignites on impact, dealing massive damage to anything caught in the blast.
6	349	353	354	358	Ancient Arrow	50	50	Special: one-shots Guardians if you shoot the eye.	An arrow created using ancient technology. To be struck with one is to be consigned to oblivion in an instant. It deals devastating damage—even against Guardians.
7	\N	\N	\N	\N	Light Arrow	0	0	None	None
\.


--
-- Data for Name: bows; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.bows (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, attack_power, durability, range, fire_rate, locations, dropped_by, description) FROM stdin;
1	332	336	337	341	Ancient Bow	44	120	50	1	\N	\N	This bow is the result of Robbie's research. Ancient Sheikah technology allows it heightened functionality, Arrows fired from it travel in a perfectly straight line.
2	334	338	339	343	Boko Bow	4	16	20	1	\N	\N	A basic Bokoblin bow made of wood. It's made by taking any tree branch and tying a string to either end, so don't expect much in the way of combat effectiveness.
3	319	323	324	328	Bow of Light	100	100	500	1	\N	\N	Princess Zelda gave you this bow and arrow for the battle with Dark beast Ganon. When wielded by the hero, it fires arrows of pure light strong enough to oppose the Calamity.
4	336	340	341	345	Dragon Bone Boko Bow	24	30	20	1	\N	\N	A Boko bow reinforced by fossils. Bokoblins handpicked the materials it's made from, so it boasts a respectable firepower.
5	343	347	348	352	Duplex Bow	14	18	40	2	\N	\N	A bow favored by the skilled archers of the Yiga Clan. It's been engineered to fire two arrows at once to ensure your target comes to a swift and none-too-pleasant halt.
6	328	332	333	337	Falcon Bow	20	50	40	1	\N	\N	A highly refined Rito-made bow created by a master Rito craftsman. Rito warriors favor it for its superior rate of fire, which helps them excel even further at aerial combat.
7	325	329	330	334	Forest Dweller's Bow	15	35	20	3	\N	\N	The Koroks made this bow for Hylians. It's crafted from flexible wood and uses sturdy vines for the bowstring. Its construction may be simple, but it fires multiple arrows at once.
8	330	334	335	339	Golden Bow	14	60	40	1	\N	\N	This Gerudo-made bow is popular for the fine ornamentations along its limbs. Designed for hunting and warfare alike, this bow was engineered to strike distant targets.
9	329	333	334	338	Great Eagle Bow	28	60	40	3	\N	\N	A bow without equal wielded by the Rito Champion, Revali. it's said Revali could loose arrows with the speed of a gale, making him supreme in aerial combat.
10	323	327	328	332	Knight's Bow	26	48	20	1	\N	\N	The sturdy metal construction of this bow offers superior durability, while its lack of firing quirks makes it quite reliable. Once favored by the knights at Hyrule Castle.
11	337	341	342	346	Lizal Bow	14	25	20	1	\N	\N	A wooden bow created by Lizalfos. It's reinforced by the bones of a large fish - a marked improvement over any standard wooden bow.
12	340	344	345	349	Lynel Bow	10	30	20	3	\N	\N	A Lynel-made bow crafted from rough metal. True to the vicious nature of Lynel weaponry. It fires a spread of multiple arrows at once. Ideal for taking down quick-moving targets.
13	341	345	346	350	Mighty Lynel Bow	20	35	20	3	\N	\N	This massive Lynel bow sports a bowstring made from a metal so tough, mere Hylians have trouble drawing it back.
14	331	335	336	340	Phrenic Bow	10	45	40	1	\N	\N	A bow passed down through the Sheikah tribe. Concentrating before drawing the string will allow you to target distant enemies as easily as those nearby.
15	338	342	343	347	Reinforced Lizal Bow	25	35	20	1	\N	\N	A Lizal bow with a grip reinforced by metal. The body is made from the branches of a flexible tree that grows near water, which offers some serious destructive power.
16	324	328	329	333	Royal Bow	38	60	20	1	\N	\N	In the past, the king of Hyrule presented this bow to only the most talented archers in the land. Its combat capabilities are as impressive as its extravagant design.
17	333	337	338	342	Royal Guard's Bow	50	20	30	1	\N	\N	This prototype Sheikah-made bow was designed to fight the Great Calamity. made with anient technology, it boasts a high rate of fire and firepower but has low durability.
18	342	346	347	351	Savage Lynel Bow	32	45	20	3	\N	\N	This Lynel bow is made from a special steel found at the peak of Death Mountain. It has tremendous stopping power and can pierce thick armor as easily as thin paper.
19	326	330	331	335	Silver Bow	15	40	20	1	\N	\N	A bow favored by the Zora for fishing. It doesn't boast the highest firepower, but the special metal it's crafted from prioritizes durability.
20	322	326	327	331	Soldier's Bow	14	36	20	1	\N	\N	A bow designed for armed conflict. Inflicts more damage than a civilian bow, but it will still burn if it touches fire.
21	335	339	340	344	Spiked Boko Bow	12	20	20	1	\N	\N	An upgraded Boko Bow bound with animal bone to boost its durability and firepower. Its craftsmanship is sloppy, but it's light and easy to use.
22	339	343	344	348	Steel Lizal Bow	36	50	20	1	\N	\N	This bow is wielded by lizalfos who are expert marksmen. The metal that reinforces much of the weapon adds some additional weight but offers heightened durability.
23	327	331	332	336	Swallow Bow	9	30	40	1	\N	\N	This bow is a favorite among Rito warriors. The bowstring has been specially engineered for aerial combat, which allows it to be drawn faster than a normal bow.
24	321	325	326	330	Traveler's Bow	5	22	20	1	\N	\N	A small bow used by travelers for protection. It doesn't do a lot of damage, but it can be used to attack foes from a distance.
25	\N	\N	\N	\N	Twilight Bow	30	100	8000	1	\N	Super Smash Bros. series Zelda amiibo	A bow used by the princess who fought the beasts of twilight alongside the hero. It's said to contain the spirits of light's power. It fires arrows straight and true, as if beams of light.
26	320	324	325	329	Wooden Bow	4	20	20	1	\N	\N	This wooden bow may not be the most reliable for battling monsters, but it is excellent for hunting small animals.
\.


--
-- Data for Name: creatures; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.creatures (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, creature_type, recoverable_materials, description) FROM stdin;
1	1	1	1	1	Horse	mammal	\N	These can be most often be found on plains. Their usefulness as transportation has made them valuable since ancient times. That said, wild horses do tend to get spooked and run off when approached, so if you're looking to snag one, it's best to sneak up on it.
2	2	2	2	2	Giant Horse	mammal	\N	This giant horse is the last of its kind. Its physical capabilities completely overshadow those of regular horses, but its temperament is extremely wild. Only a truly skilled rider can train, or even catch, this beast of a mount.
3	3	3	3	3	White Horse	mammal	\N	The Hyrulean royal family that perished 100 years ago would sometimes ride atop white horses as a display of their divine right. This white horse may be a descendant of one once ridden by royalty.
4	4	4	4	4	Lord of the Mountain	mammal	\N	This noble creature watches over all animals that make their homes in the forest. Legends say this holy creature is a reincarnation of a sage that died on the lands it now protects. It has an acute awareness of its surroundings, so it seldom appears before people. It's sometimes known by its other name, Satori.
5	5	5	5	5	Stalhorse	mammal	\N	This skeletal horse is ridden by monsters. It was once a regular horse, but Ganon's power revived it from death. It cannot maintain its bone structure in the daytime.
6	6	6	6	6	Donkey	mammal	\N	Smaller than horses, these are raised as livestock in the countryside, so they don't exist in the wild. They're more powerful than they look and specialize in transporting baggage. This has made them popular with traveling merchants.
7	7	7	7	7	Sand Seal	mammal	\N	These seals use their large flippers to move through the sand as if swimming. They were once wild animals but have since been domesticated by the Gerudo. They rely on their excellent hearing to find their way while submerged in the sand. Their large, distinct tusks look pretty ferocious, but their favorite food is actually fruit.
8	8	8	8	8	Patricia	mammal	\N	This is Riju's own sand seal. It may look intense, but she dotes on it regularly; the ribbon it wears was a gift from her, and it even has its own pen in Gerudo Town. It's far more agile than any other sand seal and far more outgoing. An ever-reliable partner to Riju, Patricia is always ready to take off through the desert at a moment's notice.
9	9	9	9	9	Bushy-Tailed Squirrel	mammal	{72}	These small creatures are known for their large tails. They tend to live in forests and subsist on acorns and other nuts. They carry food in their mouths but will sometimes drop it when surprised in an adorable display of shock.
10	10	10	10	10	Woodland Boar	mammal	{127}	These medium-sized beasts can be found all throughout Hyrule. You can most often find them foraging for food in forests or meadows. Although usually docile, they won't hesitate to charge you full force if you get too close.
11	11	11	11	11	Red-Tusked Boar	mammal	{128}	These boars are known for their red tusks and black fur. They're similar to your average boars but are considerably stronger. Extra caution is advised when hunting these.
12	12	12	12	12	Mountain Goat	mammal	{127}	These herbivores make their home in meadows and rocky areas. Their characteristically strong legs and hooves facilitate easy climbing over rocks. Goats aren't very quick, so they make good targets for beginning hunters.
13	13	13	13	13	White Goat	mammal	\N	These goats are raised for their multiple uses. Their milk can be drank or made into butter, and their soft fur is used to make clothes. Domesticated goats wear bells around their necks so they can easily be found after being let out to graze.
14	14	14	14	14	Mountain Buck	mammal	{128}	These male deer are usually found deep in the forest. They're well known for their huge, branching antlers, which they'll swing in self-defense if threatened. Deer are cautious by nature so it can be tricky getting close, but if you can capture one, you may be able to take if for a ride.
15	15	15	15	15	Mountain Doe	mammal	{128}	These female deer are often found alongside a male. They're very timid animals by nature, but they tend to let their guard down when eating apples, a favorite food. This tidbit of information can be useful to hunters.
16	16	16	16	16	Water Buffalo	mammal	{126,128}	These wild cows come equipped with big, strong horns. They live off grass that grows near the waterfront. Their meat is considered to be high quality, so they're a common target among hunters. Fun fact: the domesticated Hateno cow, often raised in villages, was bred through selective breeding using these.
17	17	17	17	17	Hateno Cow	mammal	\N	Originating in Hateno Village, these cow are kept as livestock primarily fo their milk. Their horns are smaller than a water buffalo's, and they're much more docile and therefore easier to raise. Their horns are decorated with bright colors when taken out to pasture.
18	18	18	18	18	Highland Sheep	mammal	\N	Originally native to the mountainous regions, these sheep were domesticated for their wool and have since found a home in villages. Once the fluffy wool grows out, it's harvested and used to make clothing and bedding. They're calm, timid creatures and move about in small herds.
19	19	19	19	19	Grassland Fox	mammal	{127}	This breed of fox is common to grasslands and forests and is hallmarked by its fluffy, white-tipped tail. Unlike other foxes, they tend to act independently rather than form packs. Being omnivores, they hunt animals smaller than themselves and often stick to eating fruit and insects.
20	20	20	20	20	Snowcoat Fox	mammal	{126,128}	This particular breed of grassland fox makes its home in cold climates such as the Tabantha region. Its fur turned white as a means of adapting to snowy weather, serving as a natural camouflage. Because of this, spotting one in the snow takes a keen eye.
21	21	21	21	21	Maraduo Wolf	mammal	{126,128}	These wolves are not only carnivorous but can also be downright fierce. They're highly aggressive and aren't afraid to attack people. They hunt in groups and surround their prey as a means of bringing it down. That said, if one of their own is injured, the rest are wise enough to run away. They communicate via howls, so if you're wandering the forest and hear their call, you had best take care.
22	22	22	22	22	Wasteland Coyote	mammal	{126,128}	This contentious beast is native to the Gerudo region. It makes its home in the desert, so it doesn't have a problem with hot, arid climates. They do hunt other beasts and small animals but more often turn their sights on people. They're a threat to anyone traveling through the desert.
44	44	44	44	44	Islander Hawk	bird	{124}	These carnivorous birds live in plains or highlands. They use their sharp talons and beaks to catch and eat small animals and other birds. They soar calmly through the sky and search for prey from above.
23	23	23	23	23	Cold-Footed Wolf	mammal	{126,128}	This breed of maraudo wolf lives in snowy, mountainous areas, such as the Hebra Mountains. They travel in packs similar to their maraudo relatives but are considerably tougher thanks to having adapted to their harsh environment. They're made even more dangerous by their white fur, which provides a natural camouflage in the snow.
24	24	24	24	24	Tabantha Moose	mammal	{126,128}	The largest breed of deer in Hyrule, this mammal's origin was traced back to the Tabantha region. It's easily distinguished by its immense antlers, which these moose shed and regrow yearly. Their meat is tender and high quality, so it works well in a stew.
25	25	25	25	25	Great-Horned Rhinoceros	mammal	{126,128}	This animal's horn is a whopping half the size of its body. Although originally members of arid-region rhinoceroses, these migrated to colder climates and adapted to live in snowy mountains. Their hides are particularly thick, and their horns can cause some serious damage, so only experienced hunters should track these,
26	26	26	26	26	Honeyvore Bear	mammal	{126,128}	This king among animals is dangerous game for even the most seasoned hunters. They'll attack anyone who wanders into their territory regardless of the wanderer's weaponry. As their name implies, they have a natural love of honey. Extreme caution is advised when you spot one, but if you're sneaky enough and maybe just a little bit crazy... you just may be able to ride one.
27	27	27	27	27	Grizzlemaw Bear	mammal	{4,126,128}	This breed of honeyvore bear is distinguished by its gray fur. Even more ferocious than their relatives, they're commonly believed to be the most dangerous wild animal. They live in deep snow away from villages, so it's uncommon to encounter one. That said, if you do fine one, you'd be better off not picking a fight with it.
28	28	28	28	28	Hylian Retriever	mammal	\N	The native breed of this mammal varies by region, but one thing remains true: this animal has been known as "man's best friend" since ancient times. They're very clever and obedient, so aside from serving as pets, they are also put to work watching over grazing livestock. It's said that all Hylian retrievers are descendants of the dog once owned by the king of Hyrule.
29	29	29	29	29	Blupee	mammal	{149,150,151,152}	They may look like mere rabbits at a fleeting glance, but these strange and aptly names creatures glow with a mysterious blue light. While the details of their origins are entirely unknown, there is one thing we do know: these peculiar little things have a penchant for collecting rupees.
30	30	30	30	30	Common Sparrow	bird	{83}	As the name suggests, these birds are rather common around Hyrule. They mainly live in plains or forests but sometimes venture out to villages. Their diet consists of nuts and small insects. They look pretty cute when they jump around, but they don't have much love for people.
31	31	31	31	31	Red Sparrow	bird	{83}	These small birds live in the Hebra region and eat mainly nuts and wild plants. Their bright-red feathers are often used as decorations.
32	32	32	32	32	Blue Sparrow	bird	{83}	This breed of sparrow is most often seen in the Lanayru region. They get most of their food from leaves and plants that grow near water. If it's safe enough, they can be spotted bathing in puddles or shallow rivers.
33	33	33	33	33	Rainbow Sparrow	bird	{83}	These small birds hail from the Faron region. As the name suggests, their feathers are more colorful than other sparrows', which has made them a favorite among dilettantes. They feed on small rain-forest insects, but the omnivore in them loves nuts as well.
34	34	34	34	34	Sand Sparrow	bird	{83}	This desert-dwelling breed of sparrow has adapted to withstand the heat in the arid climate of the Gerudo region. Sometimes they can be spotted half-buried in the sand, holding perfectly still as a means of evading predators.
35	35	35	35	35	Golden Sparrow	bird	{83}	This breed of sparrow is native to the Eldin region. Their down is resistant to burning, an evolutionary trait produced by the harsh volcanic environment. They subsist on small insects that hide inside rocks.
36	36	36	36	36	Wood Pigeon	bird	{125}	These pigeons inhabit vast regions all throughout Hyrule, so they can often be found in forests, grasslands, or even villages. They don't have a very strong sense of awareness, so even less-skilled hunters can nab them pretty easily.
37	37	37	37	37	Rainbow Pigeon	bird	{124}	This breed of pigeon is well known for its vivid feathers. They live mainly in the northern forests and grasslands in Hyrule. They're most fond of nuts and grain but won't pass up the occasional insect or earthworm if offered. Their meat is of a higher quality than a typical wood pigeon's.
38	38	38	38	38	Hotfeather Pigeon	bird	{124}	This rare breed of pigeon lives near Death Mountain. Their fireproof feathers are valuable as clothing material. Their meat, on the other hand, is not fireproof, and it'll burn up as soon as it's struck.
39	39	39	39	39	White Pigeon	bird	{124,129}	This white-feathered breed of pigeon lives in the Hebra region. It's said that a wood pigeon survived atop a snowy mountain by changing its feathers to blend in with the snow and soon after became a white pigeon. They have an extra layer of fat to guard against the cold, so they have been known to yield high-quality meat.
40	40	40	40	40	Mountain Crow	bird	{125}	This bird is known for its cleverness and for its distinct claws. They make their homes in mountains, forests, and villages alike. Not only are their jet-black feathers seen as an ill omen, but they'll steal the crops right out of a farmer's hands if given the chance. Because of this, they're often met with enmity.
41	41	41	41	41	Bright-Chested Duck	bird	{125}	These birds can be found on waterfronts all over Hyrule. Their webbed feet make them excellent swimmers, and their long necks allow them to duck their heads underwater to find food. Their fatty meat is high quality.
42	42	42	42	42	Blue-Winged Heron	bird	{125}	These birds live on waterfronts all throughout Hyrule. Their characteristically thin legs and long necks help them catch water-dwelling fish or frogs for food. They lie perfectly still in wait for their prey and then stretch out their long necks to capture their meat.
43	43	43	43	43	Pink Heron	bird	{125}	This breed of heron is characterized by the pink tips of its feathers. Unlike its waterfront-dwelling relative the blue-winged heron, these live in grasslands or arid regions and subsist on insects that live in the grass. They can be found walking about searching for food but will fly away if they sense danger.
45	45	45	45	45	Seagull	bird	{125}	These birds live near the ocean. They eat mainly fish, so a flock of seagulls hovering over water is a good indication of where there are fish. Fishermen use this to their advantage when searching for a catch of their own.
46	46	46	46	46	Eldin Ostrich	bird	{124,129}	This large bird makes its home in the volcanic region of Eldin. Their heat-resistant feathers and skin are evolutionary traits produced by the harsh environment. Their wings are too small to facilitate flight, but their long legs make them pretty speedy runners.
47	47	47	47	47	Cucco	bird	{79}	Villages often raise these birds for their eggs, but some people actually keep them as pets. They can't fly but can flap their wings mighty furiously if picked up. They're usually calm creature, but if you're persistent in you're torment of them... Well...
48	48	48	48	48	Hyrule Bass	fish	\N	An ordinary fish that can be found all over Hyrule. It can be eaten raw, but cooking it amplifies its healing benefits.
49	49	49	49	49	Hearty Bass	fish	\N	This large fish lives near the shoreline. Its sizable body can store a lot of nutrients. When cooked into a dish, it will temporarily increase your maximum hearts
50	50	50	50	50	Staminoka Bass	fish	\N	This Hyrule bass got to be the biggest fish by never getting caught (until now). Its long life results in a cooked dish that will restore a lot of stamina.
51	51	51	51	51	Hearty Salmon	fish	\N	This fish makes its home in cold water, giving it extra layers of fat. It temporarily increases your maximum hearts when used in cooking.
52	52	52	52	52	Chillfin Trout	fish	\N	This blue trout prefers cold bodies of water. Its skin contains enzymes that keep its body cool, and when cooked into a dish, it will temporarily boost your heat resistance.
53	53	53	53	53	Sizzlefin Trout	fish	\N	This red trout prefers warm bodies of water. It has a special organ specifically for storing heat. When cooked into a dish, it temporarily boosts your resistance to the cold.
54	54	54	54	54	Voltfin Trout	fish	\N	This trout makes its home in the freshwater lakes. Its scales have an insulating compound that, when cooked into a dish, offers resistance to electricity.
55	55	55	55	55	Stealthfin Trout	fish	\N	Consuming the bioluminescent compound that makes this fish glow in the dark will increase concentration. Dishes cooked with it will suppress noise when consumed.
56	56	56	56	56	Mighty Carp	fish	\N	This freshwater fish lives alongside its less mighty carp ilk. A compound in its liver promotes muscle growth. Dishes cooked with it will temporarily increase your attack power.
57	57	57	57	57	Armored Carp	fish	\N	Calcium deposits in the scales of this ancient fish make them hard as armor. Cooking it into a dish will fortify your bones, temporarily increasing your defense.
58	58	58	58	58	Sanke Carp	fish	\N	This wild armored carp has been bred into a prizewinning fish. Its beautifully colored markings do not occur in nature. It offers no special effects when cooked.
59	59	59	59	59	Mighty Porgy	fish	\N	This ocean-dwelling fish comes with one rude attitude. The compounds in its flesh elevate your competitive spirit when it's cooked into a dish, thus increased your attack power.
60	60	60	60	60	Armored Porgy	fish	\N	This porgy's body is covered in armor-hard scales. The compounds in its scales, when cooked into a dish, fortify your bones and temporarily boost your defense.
61	61	61	61	61	Sneaky River Snail	snail	\N	This large, glow-in-the-dark snail lives in fresh water. When cooked into a dish, it heightens your sense so you can move about silently.
62	62	62	62	62	Hearty Blueshell Snail	snail	\N	This snail lives on sandy beaches in large numbers. Its flesh contains a high amount of stimulants, so when cooked into a dish, it will temporarily increase your maximum hearts.
63	63	63	63	63	Razorclaw Crab	crab	\N	This crab is well known for its exceptionally sharp pincers. When cooked, the strength compound in its claws will increase your attack power.
64	64	64	64	64	Ironclaw Crab	crab	\N	This crab's shell is particularly hard. When cooked into a dish, its fat and meat bolster the body to increase your defense.
65	65	65	65	65	Bright-Eyed Crab	crab	\N	This crab appears in large numbers when it rains. One bite of its delectable meat, and you'll forget all your exhaustion. Replenishes your stamina when cooked into a dish.
66	66	66	66	66	Fairy	other	\N	This fairy will fly from your pouch and heal all your wounds the moment you lose your last heart. It's easily mistaken for a firefly at first, but it glows in the daylight as well as night.
67	67	67	67	67	Winterwing Butterfly	insect	\N	The powdery scales of this butterfly's wings cool the air around it. Watching it flutter around snowflakes is a thing of beauty. Cook it with monster parts for a heat-resistant elixir.
68	68	68	68	68	Summerwing Butterfly	insect	\N	A butterfly found in the woods and plains of warm regions. Its wings absorb the warmth of the sun. Cook it with monster parts to create and elixir that makes you feel warm and fuzzy.
69	69	69	69	69	Thunderwing Butterfly	insect	\N	This rare butterfly only shows itself when it rains. The organs in its body produce an insulating compound. When made into an elixir, it offers electrical resistance.
70	70	70	70	70	Smotherwing Butterfly	insect	\N	This rare butterfly lives in volcanic regions. Its body contains a heat-resistance liquid, which can be turned into a topical elixir that offers resistance to flames.
71	71	71	71	71	Cold Darner	insect	\N	This dragonfly prefers the cool shade of trees to the warmth of the sun. Its wings disperse heat from its body, which can be cooked into a heat-resistance elixir.
72	72	72	72	72	Warm Darner	insect	\N	This dragonfly has a special organ that causes it to sweat profusely. Cook it with monster parts for an elixir that will raise your core temperature so you can resist the cold.
73	73	73	73	73	Electric Darner	insect	\N	This rare dragonfly only appears in the rain. Its wings direct electricity away from its body. Cook it with monster parts for an electricity-resistance elixir.
74	74	74	74	74	Restless Cricket	insect	\N	A very energetic cricket. Cook it with monster parts to create a stamina-recovery elixir.
75	75	75	75	75	Bladed Rhino Beetle	insect	\N	This beetle's razor-sharp horns demand that you handle it with care. Boil the horns alongside monster parts to concoct an elixir that will raise your attack power.
76	76	76	76	76	Rugged Rhino Beetle	insect	\N	This beetle's hard body resembles armor. When the shell is cooked with monster parts, the resulting elixir boosts your defense.
77	77	77	77	77	Energetic Rhino Beetle	insect	\N	This valuable beetle can live up to ten years. When cooked with monster parts, its impressive vitality translates into an elixir that will greatly restore your stamina.
78	78	78	78	78	Sunset Firefly	insect	\N	These fireflies glow gently in the dark. When cooked with monster parts, the compound that causes it to glow results in an elixir that will allow you to move more quietly.
79	79	79	79	79	Hot-Footed Frog	amphibian	\N	A quick frog that can be found hopping around near water. Cook it with monster parts to draw out its speed-boost effect.
80	80	80	80	80	Tireless Frog	amphibian	\N	This rare frog only ventures out in the rain. When cooked with monster parts, the elixir it produces will temporarily increase your maximum stamina.
81	81	81	81	81	Hightail Lizard	reptile	\N	A lizard found throughout Hyrule. It's a bit slow to react at time, but if given a chance to escape, it will dart off quickly. Cook it with monster parts for a speed-boosting elixir.
82	82	82	82	82	Hearty Lizard	reptile	\N	This rare lizard lives deep in forest. It feed on high-nutrient foods, giving it great vitality. When used to make elixirs, they temporarily increase your maximum hearts.
83	83	83	83	83	Fireproof Lizard	reptile	\N	This rare lizard can only be found in the Eldin region. Its scale have heat-resistant properties, so when cooked with monster parts, it produces a heat-resistance elixir.
\.


--
-- Data for Name: location_types; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.location_types (id, name) FROM stdin;
1	shrine
2	stable
3	village
4	tower
5	fountain
6	spring
\.


--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.locations (id, name, region_fk, subregion_fk, location_type_fk) FROM stdin;
1	Akkala Bridge Ruins	1	1	\N
2	Akkala Citadel Ruins	1	1	\N
3	Akkala Falls	1	1	\N
4	Akkala Parade Ground Ruins	1	1	\N
5	Akkala Span	1	1	\N
6	Akkala Tower	1	1	4
7	Dah Hesho Shrine	1	1	1
8	East Akkala Plains	1	1	\N
9	East Sokkala Bridge	1	1	\N
10	Great Fairy Fountain	1	1	5
11	Kaepora Pass	1	1	\N
12	Kanalet Ridge	1	1	\N
13	Ke'nai Shakah Shrine	1	1	1
14	Lake Akkala	1	1	\N
15	Shadow Hamlet Ruins	1	1	\N
16	Shadow Pass	1	1	\N
17	Sokkala Bridge	1	1	\N
18	South Akkala Plains	1	1	\N
19	South Akkala Stable	1	1	2
20	South Lake Akkala	1	1	\N
21	Spring of Power	1	1	6
22	Ternio Trail	1	1	\N
23	Torin Wetland	1	1	\N
24	Ukuku Plains	1	1	\N
25	Ulri Mountain	1	1	\N
26	Ulria Grotto	1	1	\N
27	West Sokkala Bridge	1	1	\N
28	Ze Kasho Shrine	1	1	1
29	East Akkala Beach	1	2	\N
30	Lomei Labyrinth Island	1	2	\N
31	Malin Bay	1	2	\N
32	North Akkala Beach	1	2	\N
33	Rist Peninsula	1	2	\N
34	Ritaag Zumo Shrine	1	2	1
35	Tu Ka'loh Shrine	1	2	1
36	Akkala Ancient Tech Lab	1	3	\N
37	Akkala Wilds	1	3	\N
38	Bloodleaf Lake	1	3	\N
39	East Akkala Stable	1	3	2
40	Katosa Aug Shrine	1	3	1
41	North Akkala Foothill	1	3	\N
42	North Akkala Valley	1	3	\N
43	Ordorac Quarry	1	3	\N
44	Rok Woods	1	3	\N
45	Skull Lake	1	3	\N
46	Tempest Gulch	1	3	\N
47	Tumlea Heights	1	3	\N
48	Tutsuwa Nima Shrine	1	3	1
49	Zuna Kai Shrine	1	3	1
50	Inn	1	4	3
51	Ore and More	1	4	3
52	Rhondson Armor Boutique	1	4	3
53	Slippery Falcon	1	4	3
54	Ancient Tree Stump	2	5	\N
55	Applean Forest	2	5	\N
56	Aquame Bridge	2	5	\N
57	Aquame Lake	2	5	\N
58	Boneyard Bridge	2	5	\N
59	Bottomless Swamp	2	5	\N
60	Carok Bridge	2	5	\N
61	Castle Town Prison	2	5	\N
62	Castle Town Watchtower	2	5	\N
63	Central Square	2	5	\N
64	Central Tower	2	5	4
65	Coliseum Ruins	2	5	\N
66	Crenel Hills	2	5	\N
67	Dah Kaso Shrine	2	5	1
68	Digdogg Suspension Bridge	2	5	\N
69	East Castle Town	2	5	\N
70	East Post Ruins	2	5	\N
71	Exchange Ruins	2	5	\N
72	Forest of Time	2	5	\N
73	Gatepost Town Ruins	2	5	\N
74	Giant's Forest	2	5	\N
75	Gleeok Bridge	2	5	\N
76	Helmhead Bridge	2	5	\N
77	Hyrule Castle Moat	2	5	\N
78	Hyrule Castle Town Ruins	2	5	\N
79	Hyrule Cathedral	2	5	\N
80	Hyrule Forest Park	2	5	\N
81	Hyrule Garrison Ruins	2	5	\N
82	Kaam Ya'tak Shrine	2	5	1
83	Katah Chuki Shrine	2	5	1
84	Kolomo Garrison Ruins	2	5	\N
85	Lake Kolomo	2	5	\N
86	Mabe Prairie	2	5	\N
87	Mabe Village Ruins	2	5	\N
88	Manhala Bridge	2	5	\N
89	Moat Bridge	2	5	\N
90	Mount Daphnes	2	5	\N
91	Mount Gustaf	2	5	\N
92	Namika Ozz Shrine	2	5	1
93	Noya Neha Shrine	2	5	1
94	Orsedd Bridge	2	5	\N
95	Outskirt Stable	2	5	2
96	Passeri Greenbelt	2	5	\N
97	Quarry Ruins	2	5	\N
98	Ranch Ruins	2	5	\N
99	Regencia River	2	5	\N
100	Riverside Stable	2	5	2
101	Romani Plains	2	5	\N
102	Rota Ooh Shrine	2	5	1
103	Sacred Ground Ruins	2	5	\N
104	Sage Temple Ruins	2	5	\N
105	Wahgo Katta Shrine	2	5	1
106	Water Reservoir	2	5	\N
107	West Castle Town	2	5	\N
108	Whistling Hill	2	5	\N
109	Windvane Meadow	2	5	\N
110	Dining Hall	2	6	\N
111	Docks	2	6	\N
112	East Passage	2	6	\N
113	First Gatehouse	2	6	\N
114	Guards' Chamber	2	6	\N
115	King's Study	2	6	\N
116	Library	2	6	\N
117	Lockup	2	6	\N
118	Observation Room	2	6	\N
119	Princess Zelda's Room	2	6	\N
120	Princess Zelda's Study	2	6	\N
121	Sanctum	2	6	\N
122	Hyrule Castle Underground	2	6	\N
123	Laboratory	2	6	\N
124	Saas Ko'sah Shrine	2	6	1
125	Second Gatehouse	2	6	\N
126	West Passage	2	6	\N
127	Ash Swamp	3	7	\N
128	Batrea Lake	3	7	\N
129	Big Twin Bridge	3	7	\N
130	Blatchery Plain	3	7	\N
131	Bonooru's Stand	3	7	\N
132	Bosh Kala Shrine	3	7	1
133	Bubinga Forest	3	7	\N
134	Dueling Peaks Stable	3	7	2
135	Dueling Peaks Tower	3	7	4
136	Eagus Bridge	3	7	\N
137	Floret Sandbar	3	7	\N
138	Great Fairy Fountain	3	7	5
139	Ha Dahamar Shrine	3	7	1
140	Hickaly Woods	3	7	\N
141	Hila Rao Shrine	3	7	1
142	Hills of Baumer	3	7	\N
143	Horwell Bridge	3	7	\N
144	Hylia River	3	7	\N
145	Kakariko Bridge	3	7	\N
146	Lake Siela	3	7	\N
147	Lakna Rokee Shrine	3	7	1
148	Lantern Lake	3	7	\N
149	Little Twin Bridge	3	7	\N
150	Mable Ridge	3	7	\N
151	Mount Rozudo	3	7	\N
152	Nabi Lake	3	7	\N
153	Oakle's Navel	3	7	\N
154	Outpost Ruins	3	7	\N
155	Owlan Bridge	3	7	\N
156	Pillars of Levia	3	7	\N
157	Proxim Bridge	3	7	\N
158	Ree Dahee Shrine	3	7	1
159	Sahasra Slope	3	7	\N
160	Scout's Hill	3	7	\N
161	Shee Vaneer Shrine	3	7	1
162	Shee Venath Shrine	3	7	1
163	South Nabi Lake	3	7	\N
164	Squabble River	3	7	\N
165	Ta'loh Naeg Shrine	3	7	1
166	Toto Sah Shrine	3	7	1
167	Enchanted	3	8	\N
168	Fang and Bone	3	8	\N
169	High Spirits Produce	3	8	\N
170	Shuteye Inn	3	8	\N
171	The Curious Quiver	3	8	\N
172	Abandoned North Mine	4	9	\N
173	Broca Island	4	9	\N
174	Cephla Lake	4	9	\N
175	Darunia Lake	4	9	\N
176	Eldin Tower	4	9	4
177	Foothill Stable	4	9	2
178	Gero Pond	4	9	\N
179	Golow River	4	9	\N
180	Gorae Torr Shrine	4	9	1
181	Gorko Tunnel	4	9	\N
182	Goronbi Lake	4	9	\N
183	Goronbi River	4	9	\N
184	Gortram Cliff	4	9	\N
185	Gut Check Rock	4	9	\N
186	Isle of Rabac	4	9	\N
187	Kayra Mah Shrine	4	9	1
188	Lake Ferona	4	9	\N
189	Lake Intenoch	4	9	\N
190	Maw of Death Mountain	4	9	\N
191	Medingo Pool	4	9	\N
192	Mo'a Keet Shrine	4	9	1
193	Qua Raym Shrine	4	9	1
194	Sah Dahaj Shrine	4	9	1
195	Shora Hah Shrine	4	9	1
196	Southern Mine	4	9	\N
197	Tah Muhl Shrine	4	9	1
198	Trilby Valley	4	9	\N
199	Bridge of Eldin	4	10	\N
200	Daqa Koh Shrine	4	10	1
201	Darb Pond	4	10	\N
202	Death Caldera	4	10	\N
203	Gorko Lake	4	10	\N
204	Goro Cove	4	10	\N
205	Goron Hot Springs	4	10	\N
206	Lake Darman	4	10	\N
207	Eldin Great Skeleton	4	11	\N
208	Eldin's Flank	4	11	\N
209	Goron Gusto Shop	4	12	\N
210	Protein Palace	4	12	\N
211	Ripped and Shredded	4	12	\N
212	Rollin' Inn	4	12	\N
213	Shae Mo'sah Shrine	4	12	1
214	Stolock Bridge	4	12	\N
215	Angel Peak	5	13	\N
216	Aris Beach	5	13	\N
217	Atun Valley	5	13	\N
218	Breman Peak	5	13	\N
219	Bronas Forest	5	13	\N
220	Calora Lake	5	13	\N
221	Cape Cales	5	13	\N
222	Corta Lake	5	13	\N
223	Courage Steppe	5	13	\N
224	Dunsel Plateau	5	13	\N
225	Ebara Forest	5	13	\N
226	Faron Tower	5	13	4
227	Floria Bridge	5	13	\N
228	Floria Falls	5	13	\N
229	Gama Cove	5	13	\N
230	Hanu Pond	5	13	\N
231	Jia Highlands	5	13	\N
232	Kah Yah Shrine	5	13	1
233	Kamah Plateau	5	13	\N
234	Keelay Plain	5	13	\N
235	Keya Pond	5	13	\N
236	Koto Pond	5	13	\N
237	Lake Floria	5	13	\N
238	Lakeside Stable	5	13	2
239	Meda Mountain	5	13	\N
240	Mount Dunsel	5	13	\N
241	Mount Floria	5	13	\N
242	Mount Taran	5	13	\N
243	Muwo Jeem Shrine	5	13	1
244	Palmorae Beach	5	13	\N
245	Palmorae Ruins	5	13	\N
246	Qukah Nata Shrine	5	13	1
247	Rabella Wetlands	5	13	\N
248	Rassla Lake	5	13	\N
249	Riola Spring	5	13	\N
250	Rodai Lake	5	13	\N
251	Sarjon Bridge	5	13	\N
252	Sarjon Woods	5	13	\N
253	Shai Utoh Shrine	5	13	1
254	Shoda Sah Shrine	5	13	1
255	Stinger Cliffs	5	13	\N
256	Taran Pass	5	13	\N
257	Tawa Jinn Shrine	5	13	1
258	Temto Hill	5	13	\N
259	Tobio's Hollow	5	13	\N
260	Tuft Mountain	5	13	\N
261	Ubota Point	5	13	\N
262	Uten Marsh	5	13	\N
263	Yah Rin Shrine	5	13	1
264	Yambi Lake	5	13	\N
265	General Store	5	14	\N
266	Inn	5	14	\N
267	Aris Beach	5	15	\N
268	Cape Cresia	5	15	\N
269	Clarnet Coast	5	15	\N
270	Eventide Island	5	15	\N
271	Gogobi Shores	5	15	\N
272	Koholit Rock	5	15	\N
273	Korgu Chideh Shrine	5	15	1
274	Korne Beach	5	15	\N
275	Martha's Landing	5	15	\N
276	Rimba Beach	5	15	\N
277	Soka Point	5	15	\N
278	Toronbo Beach	5	15	\N
279	Birida Lookout	6	16	\N
280	Cliffs of Ruvara	6	16	\N
281	Gerudo Summit	6	16	\N
282	Gerudo Tower	6	16	4
283	Hemaar's Descent	6	16	\N
284	Joloo Nah Shrine	6	16	1
285	Keeha Yoog Shrine	6	16	1
286	Kema Kosassa Shrine	6	16	1
287	Kuh Takkar Shrine	6	16	1
288	Laparoh Mesa	6	16	\N
289	Meadela's Mantle	6	16	\N
290	Mount Agaat	6	16	\N
291	Mystathi's Shelf	6	16	\N
292	Nephra Hill	6	16	\N
293	Risoka Snowfield	6	16	\N
294	Rutimala Hill	6	16	\N
295	Sapphia's Table	6	16	\N
296	Sasa Kai Shrine	6	16	1
297	Sho Dantu Shrine	6	16	1
298	Statue of the Eighth Heroine	6	16	\N
299	Taafei Hill	6	16	\N
300	Vatorsa Snowfield	6	16	\N
301	Zirco Mesa	6	16	\N
302	Arbiter's Grounds	7	17	\N
303	Champion's Gate	7	17	\N
304	Dako Tah Shrine	7	17	1
305	Daqo Chisay Shrine	7	17	1
306	Daval Peak	7	17	\N
307	Dila Maag Shrine	7	17	1
308	Dragon's Exile	7	17	\N
309	East Barrens	7	17	\N
310	East Gerudo Mesa	7	17	\N
311	East Gerudo Ruins	7	17	\N
312	Gerudo Canyon	7	17	\N
313	Gerudo Canyon Pass	7	17	\N
314	Gerudo Canyon Stable	7	17	2
315	Gerudo Desert Gateway	7	17	\N
316	Gerudo Great Skeleton	7	17	\N
317	Great Cliffs	7	17	\N
318	Great Fairy Fountain	7	17	5
319	Hawa Koth Shrine	7	17	1
320	Jee Noh Shrine	7	17	1
321	Kara Kara Bazaar	7	17	\N
322	Karusa Valley	7	17	\N
323	Kay Noh Shrine	7	17	1
324	Kema Zoos Shrine	7	17	1
325	Korsh O'hu Shrine	7	17	1
326	Koukot Plateau	7	17	\N
327	Misae Suma Shrine	7	17	1
328	Mount Granajh	7	17	\N
329	Mount Nabooru	7	17	\N
330	Northern Icehouse	7	17	\N
331	Palu Wasteland	7	17	\N
332	Raqa Zunzo Shrine	7	17	1
333	Sand-Seal Rally	7	17	\N
334	South Lomei Labyrinth	7	17	\N
335	Southern Oasis	7	17	\N
336	Spectacle Rock	7	17	\N
337	Stalry Plateau	7	17	\N
338	Suma Sahma Shrine	7	17	1
339	Taafei Hill	7	17	\N
340	Tho Kayu Shrine	7	17	1
341	Toruma Dunes	7	17	\N
342	Wasteland Tower	7	17	4
343	West Barrens	7	17	\N
344	West Gerudo Ruins	7	17	\N
345	Yarna Valley	7	17	\N
346	Yiga Clan Hideout	7	17	\N
347	Arrow Specialty Shop	7	18	\N
348	Fashion Passion	7	18	\N
349	Hotel Oasis	7	18	\N
350	Sand-Seal Rental Shop	7	18	\N
351	Starlight Memories	7	18	\N
352	The Noble Canteen	7	18	\N
353	Aldor Foothills	8	19	\N
354	Deplian Badlands	8	19	\N
355	Drenan Highlands	8	19	\N
356	East Deplian Badlands	8	19	\N
357	Elma Knolls	8	19	\N
358	Forgotten Temple	8	19	\N
359	Ketoh Wawai Shrine	8	19	1
360	Maritta Exchange Ruins	8	19	\N
361	Military Training Camp	8	19	\N
362	Minshi Woods	8	19	\N
363	Mirro Shaz Shrine	8	19	1
364	Mount Drena	8	19	\N
365	Monya Toma Shrine	8	19	1
366	Pico Pond	8	19	\N
367	Rauru Hillside	8	19	\N
368	Rauru Settlement Ruins	8	19	\N
369	Rona Kachta Shrine	8	19	1
370	Rowan Plain	8	19	\N
371	Salari Hill	8	19	\N
372	Salari Plain	8	19	\N
373	Serenne Stable	8	19	2
374	Thyphlo Ruins	8	19	\N
375	Trilby Plain	8	19	\N
376	West Deplian Badlands	8	19	\N
377	Woodland Stable	8	19	2
378	Woodland Tower	8	19	4
379	Daag Chokah Shrine	8	20	1
380	Keo Ruug Shrine	8	20	1
381	Korok Forest	8	20	\N
382	General Shoppe	8	20	\N
383	Great Deku Tree's Navel	8	20	\N
384	Spore Store	8	20	\N
385	Kuhn Sidajj Shrine	8	20	1
386	Lake Mekar	8	20	\N
387	Lake Saria	8	20	\N
388	Lost Woods	8	20	\N
389	Maag Halan Shrine	8	20	1
390	Mekar Island	8	20	\N
391	Mido Swamp	8	20	\N
392	Eastern Abbey	9	21	\N
393	Forest of Spirits	9	21	\N
394	Great Plateau Tower	9	21	4
395	Hopper Pond	9	21	\N
396	Ja Baij Shrine	9	21	1
397	Keh Namut Shrine	9	21	1
398	Mount Hylia	9	21	\N
399	Oman Au Shrine	9	21	1
400	Owa Daim Shrine	9	21	1
401	River of the Dead	9	21	\N
402	Shrine of Resurrection	9	21	1
403	Temple of Time	9	21	\N
404	Camphor Pond	10	22	\N
405	Cliffs of Quince	10	22	\N
406	Dow Na'eh Shrine	10	22	1
407	Ebon Mountain/Lovers Pond	10	22	\N
408	Fir River	10	22	\N
409	Firly Plateau	10	22	\N
410	Firly Pond	10	22	\N
411	Fort Hateno	10	22	\N
412	Ginner Woods	10	22	\N
413	Hateno Ancient Tech Lab	10	22	\N
414	Hateno Tower	10	22	4
415	Jitan Sa'mi Shrine	10	22	1
416	Kam Urog Shrine	10	22	1
417	Lake Jarrah	10	22	\N
418	Lake Sumac	10	22	\N
419	Lanayru Bluff	10	22	\N
420	Lanayru Heights	10	22	\N
421	Lanayru Promenade	10	22	\N
422	Lanayru Range	10	22	\N
423	Lanayru Road - East Gate	10	22	\N
424	Lanayru Road - West Gate	10	22	\N
425	Madorna Mountain	10	22	\N
426	Marblod Plain	10	22	\N
427	Mezza Lo Shrine	10	22	1
428	Midla Woods	10	22	\N
429	Myahm Agana Shrine	10	22	1
430	Naydra Snowfield	10	22	\N
431	Nirvata Lake	10	22	\N
432	Nirvata Plateau	10	22	\N
433	Ovli Plain	10	22	\N
434	Peak of Awakening	10	22	\N
435	Phalian Highlands	10	22	\N
436	Pierre Plateau	10	22	\N
437	Purifier Lake	10	22	\N
438	Quatta's Shelf	10	22	\N
439	Rabia Plain	10	22	\N
440	Retsam Forest	10	22	\N
441	Robred Dropoff	10	22	\N
442	Solewood Range	10	22	\N
443	Spring of Wisdom	10	22	6
444	Tahno O'ah Shrine	10	22	1
445	Trotter's Downfall	10	22	\N
446	Walnot Mountain	10	22	\N
447	Zelkoa Pond	10	22	\N
448	East Wind	10	23	\N
449	Kochi Dye Shop	10	23	\N
450	Link's House	10	23	\N
451	The Great Ton Pu Inn	10	23	\N
452	Ventest Clothing Boutique	10	23	\N
453	Chaas Qeta Shrine	10	24	1
454	Deepback Bay	10	24	\N
455	Hateno Bay	10	24	\N
456	Hateno Beach	10	24	\N
457	Kitano Bay	10	24	\N
458	Loshlo Harbor	10	24	\N
459	Mapla Point	10	24	\N
460	Tenoko Island	10	24	\N
461	Biron Snowshelf	11	25	\N
462	Coldsnap Hollow	11	25	\N
463	Corvash Peak	11	25	\N
464	Goflam's Secret Hot Spring	11	25	\N
465	Goma Asaagh Shrine	11	25	1
466	Hebra East Summit	11	25	\N
467	Hebra Great Skeleton	11	25	\N
468	Hebra North Crest	11	25	\N
469	Hebra North Summit	11	25	\N
470	Hebra Peak	11	25	\N
471	Hebra South Summit	11	25	\N
472	Hebra Tundra	11	25	\N
473	Hebra West Summit	11	25	\N
474	Hia Miu Shrine	11	25	1
475	Icefall Foothills	11	25	\N
476	Lake Kilsie	11	25	\N
477	Maka Rah Shrine	11	25	1
478	Mozo Shenno Shrine	11	25	1
479	Pikida Stonegrove	11	25	\N
480	Rok Uwog Shrine	11	25	1
481	Rospro Pass	11	25	\N
482	Selmie's Spot	11	25	\N
483	Shada Naw Shrine	11	25	1
484	Sherfin's Secret Hot Spring	11	25	\N
485	Sturnida Basin	11	25	\N
486	Sturnida Secret Hot Spring	11	25	\N
487	Talonto Peak	11	25	\N
488	To Quomo Shrine	11	25	1
489	Dunba Taag Shrine	11	26	1
490	Gee Ha'rah Shrine	11	26	1
491	Hebra Falls	11	26	\N
492	Hebra Headspring	11	26	\N
493	Hebra Tower	11	26	4
494	Kopeeki Drifts	11	26	\N
495	Lanno Kooh Shrine	11	26	1
496	N. Tabantha Snowfield	11	26	\N
497	North Lomei Labyrinth	11	26	\N
498	Pondo's Lodge	11	26	\N
499	Qaza Tokki Shrine	11	26	1
500	Rin Oyaa Shrine	11	26	1
501	S. Tabantha Snowfield	11	26	\N
502	Sha Gehma Shrine	11	26	1
503	Snowfield Stable	11	26	2
504	Tabantha Hills	11	26	\N
505	Tabantha Village Ruins	11	26	\N
506	Tama Pond	11	26	\N
507	Barula Plain	12	27	\N
508	Bridge of Hylia	12	27	\N
509	Cora Lake	12	27	\N
510	Damel Forest	12	27	\N
511	Deya Lake	12	27	\N
512	Deya Village Ruins	12	27	\N
513	Dracozu Lake	12	27	\N
514	Dracozu River	12	27	\N
515	Faron Woods	12	27	\N
516	Farosh Hills	12	27	\N
517	Finra Woods	12	27	\N
518	Fural Plain	12	27	\N
519	Grinnden Plain	12	27	\N
520	Guchini Plain	12	27	\N
521	Guchini Plain Barrows	12	27	\N
522	Haran Lake	12	27	\N
523	Harfin Valley	12	27	\N
524	Harker Lake	12	27	\N
525	Herin Lake	12	27	\N
526	Highland Stable	12	27	2
527	Horse God Bridge	12	27	\N
528	Hylia Island	12	27	\N
529	Ishto Soh Shrine	12	27	1
530	Ka'o Makagh Shrine	12	27	1
531	Lake Hylia	12	27	\N
532	Lake of the Horse God	12	27	\N
533	Lake Tower	12	27	4
534	Malanya Spring	12	27	\N
535	Menoat River	12	27	\N
536	Mount Faloraa	12	27	\N
537	Mounted Archery Camp	12	27	\N
538	Nautelle Wetlands	12	27	\N
539	Oseira Plains	12	27	\N
540	Pagos Woods	12	27	\N
541	Pappetto Grove	12	27	\N
542	Parache Plains	12	27	\N
543	Popla Foothills	12	27	\N
544	Pumaag Nitae Shrine	12	27	1
545	Shae Katha Shrine	12	27	1
546	Shoqa Tatone Shrine	12	27	1
547	Spring of Courage	12	27	6
548	Taobab Grassland	12	27	\N
549	Ya Naga Shrine	12	27	1
550	Zokassa Ridge	12	27	\N
551	Zonai Ruins	12	27	\N
552	Darybon Plains	12	28	\N
553	Ibara Butte	12	28	\N
554	Komo Shoreline	12	28	\N
555	Laverra Beach	12	28	\N
556	Nette Plateau	12	28	\N
557	Puffer Beach	12	28	\N
558	Bank of Wishes	13	29	\N
559	Brynna Plain	13	29	\N
560	East Reservoir Lake	13	29	\N
561	Great Zora Bridge	13	29	\N
562	Horon Lagoon	13	29	\N
563	Inogo Bridge	13	29	\N
564	Ja'Abu Ridge	13	29	\N
565	Lulu Lake	13	29	\N
566	Luto's Crossing	13	29	\N
567	Oren Bridge	13	29	\N
568	Ploymus Mountain	13	29	\N
569	Ralis Pond	13	29	\N
570	Rucco Maag Shrine	13	29	1
571	Rutala Dam	13	29	\N
572	Ruto Mountain	13	29	\N
573	Samasa Plain	13	29	\N
574	Shatterback Point	13	29	\N
575	Tabahl Woods	13	29	\N
576	Tal Tal Peak	13	29	\N
577	Veiled Falls	13	29	\N
578	Zodobon Highlands	13	29	\N
579	Zora River	13	29	\N
580	Afromsia Coast	13	30	\N
581	Ankel Island	13	30	\N
582	Davdi Island	13	30	\N
583	Kah Mael Shrine	13	30	1
584	Knuckel Island	13	30	\N
585	Lanayru Bay	13	30	\N
586	Lodrum Headland	13	30	\N
587	Shai Yota Shrine	13	30	1
588	Spool Bight	13	30	\N
589	Talus Plateau	13	30	\N
590	Tarm Point	13	30	\N
591	Tingel Island	13	30	\N
592	Wintre Island	13	30	\N
593	Bannan Island	13	31	\N
594	Boné Pond	13	31	\N
595	Crenel Peak	13	31	\N
596	Dagah Keek Shrine	13	31	1
597	Daka Tuss Shrine	13	31	1
598	Goponga Island	13	31	\N
599	Goponga Village Ruins	13	31	\N
600	Kaya Wan Shrine	13	31	1
601	Kincean Island	13	31	\N
602	Lanayru Tower	13	31	4
603	Linebeck Island	13	31	\N
604	Mercay Island	13	31	\N
605	Mikau Lake	13	31	\N
606	Millennio Sandbar	13	31	\N
607	Molida Island	13	31	\N
608	Moor Garrison Ruins	13	31	\N
609	Rebonae Bridge	13	31	\N
610	Rikoka Hills	13	31	\N
611	Rutala River	13	31	\N
612	Ruto Lake	13	31	\N
613	Ruto Precipice	13	31	\N
614	Sheh Rata Shrine	13	31	1
615	Shrine Island	13	31	1
616	Soh Kofi Shrine	13	31	1
617	Telta Lake	13	31	\N
618	Thims Bridge	13	31	\N
619	Toto Lake	13	31	\N
620	Upland Zorana	13	31	\N
621	Wes Island	13	31	\N
622	Wetland Stable	13	31	2
623	Zauz Island	13	31	\N
624	Zelo Pond	13	31	\N
625	Coral Reef	13	32	\N
626	Ne'ez Yohma Shrine	13	32	1
627	Seabed Inn	13	32	\N
628	Breach of Demise	14	33	\N
629	Dalite Forest	14	33	\N
630	Footrace Check-In	14	33	\N
631	Illumeni Plateau	14	33	\N
632	Irch Plain	14	33	\N
633	Jeddo Bridge	14	33	\N
634	Lake Illumeni	14	33	\N
635	Lindor's Brow	14	33	\N
636	Ludfo's Bog	14	33	\N
637	Maag No'rah Shrine	14	33	1
638	Mijah Rokee Shrine	14	33	1
639	Mogg Latan Shrine	14	33	1
640	Mount Rhoam	14	33	\N
641	Nima Plain	14	33	\N
642	North Hyrule Plain	14	33	\N
643	Ridgeland Tower	14	33	4
644	Royal Ancient Lab Ruins	14	33	\N
645	Rutile Lake	14	33	\N
646	Safula Hill	14	33	\N
647	Sanidin Park Ruins	14	33	\N
648	Satori Mountain	14	33	\N
649	Seres Scablands	14	33	\N
650	Shae Loya Shrine	14	33	1
651	Sheem Dagoze Shrine	14	33	1
652	Tabantha Bridge Stable	14	33	2
653	Tabantha Great Bridge	14	33	\N
654	Tamio River	14	33	\N
655	Tanagar Canyon	14	33	\N
656	Tanagar Canyon Course	14	33	\N
657	Thundra Plateau	14	33	\N
658	Toh Yahsa Shrine	14	33	1
659	Upland Lindor	14	33	\N
660	Washa's Bluff	14	33	\N
661	West Hyrule Plains	14	33	\N
662	Zalta Wa Shrine	14	33	1
663	Ancient Columns	15	34	\N
664	Bareeda Naag Shrine	15	34	1
665	Cuho Mountain	15	34	\N
666	Dragon Bone Mire	15	34	\N
667	Dronoc's Pass	15	34	\N
668	Flight Range	15	34	\N
669	Gisa Crater	15	34	\N
670	Great Fairy Fountain	15	34	5
671	Hebra Plunge	15	34	\N
672	Hebra Trailhead Lodge	15	34	\N
673	Kah Okeo Shrine	15	34	1
674	Kolami Bridge	15	34	\N
675	Lake Totori	15	34	\N
676	Nero Hill	15	34	\N
677	Passer Hill	15	34	\N
678	Piper Ridge	15	34	\N
679	Rayne Highlands	15	34	\N
680	Rito Stable	15	34	2
681	Rospro Pass	15	34	\N
682	Sha Warvo Shrine	15	34	1
683	Strock Lake	15	34	\N
684	Tabantha Tower	15	34	4
685	Tena Ko'sah Shrine	15	34	1
686	Voo Lota Shrine	15	34	1
687	Warbler's Nest	15	34	\N
688	Akh Va'quot Shrine	15	35	1
689	Brazen Beak	15	35	\N
690	Revali's Landing	15	35	\N
691	Slippery Falcon	15	35	\N
692	Swallow's Roost	15	35	\N
\.


--
-- Data for Name: materials; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.materials (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, material_type, value, restores, additional_uses, description) FROM stdin;
36	162	165	167	170	Apple	Food	3	0.5	\N	A common fruit found on trees all around Hyrule. Eat it fresh, or cook it to increase its effect.
37	163	166	168	171	Palm Fruit	Food	4	1	\N	Fruit from palm trees that grow near the ocean. It doesn't offer any special effects but will increase your heart recovery when used as an ingredient.
38	164	167	169	172	Wildberry	Food	3	0.5	\N	A fruit that grows in cold, snowy regions known for its tangy, sweet flavor. It doesn't offer any special effects, but it's a popular cooking ingredient.
39	165	168	170	173	Hearty Durian	Food	15	3	{26}	This fruit's odor earned it the nickname "king of fruits". It offers immense restorative power; dishes cooked with it will temporarily increase your maximum hearts.
40	166	169	171	174	Hydromelon	Food	4	0.5	{7}	This resilient fruit can flourish even in the heat of the desert. The hydrating liquid inside provides a cooling effect that, when cooked, increases your heat resistance.
41	167	170	172	175	Spicy Pepper	Food	3	0.5	{7}	This pepper is exploding with spice. Cook with it to create dishes that will raise your body temperature and help you withstand the cold.
42	168	171	173	176	Voltfruit	Food	4	0.5	{2,8}	Cacti found in the Gerudo Desert bear this sweet fruit. It's naturally insulated, so when cooked into a dish, it provides resistance against electricity.
43	169	172	174	177	Fleet-Lotus Seeds	Food	5	0.5	{29}	The plant that bears these seeds grows near deep water. The roots draw nutrients from the water, which boosts your movement speed when the seeds are cooked into a dish.
44	170	173	175	178	Mighty Bananas	Food	5	0.5	{4}	This fruit grows mainly in tropical forests of the Faron region. When it's used as an ingredient, the resulting dish will temporarily increase your attack power.
45	171	174	176	179	Hylian Shroom	Food	3	0.5	\N	A common mushroom found near trees around Hyrule. Eat it to restore half a heart.
46	172	175	177	180	Endura Shroom	Food	6	1	{27}	A rare yellowish-orange mushroom. Cook it before eating to temporarily increase your stamina limit.
47	173	176	178	181	Stamella Shroom	Food	5	1	{32}	A green mushroom that grows near trees in the forest. It's chock-full of natural energy. Cook it to release its stamina-restoration properties.
48	174	177	179	182	Hearty Truffle	Food	6	2	{26}	This rare mushroom has a rich scent. Cook it before eating to temporarily increase your maximum hearts.
49	175	178	180	183	Big Hearty Truffle	Food	15	3	{26}	Years of going unpicked have allowed this hearty truffle to grow quite large. It's chock-full of nutrients. When cooked into a dish, it temporarily increases your maximum hearts.
50	176	179	181	184	Chillshroom	Food	4	0.5	{7}	Often found at the base of pine trees in cold climates, these mushrooms are cool to the touch and can be used to cook dishes that allow you to stay cool even in arid regions.
51	177	180	182	185	Sunshroom	Food	4	0.5	{2,5}	A bright red mushroom that grows in hot climates. Imbued with the power of heat, they can be used to cook dishes that will allow you to endure the bitter cold.
52	178	181	183	186	Zapshroom	Food	4	0.5	{2,8}	This mushroom grows wild in the Gerudo region. The cap is naturally insulated, so when used in cooking, it will offer protection against electricity.
53	179	182	184	187	Rushroom	Food	3	0.5	{2,29}	A mushroom that can grow almost anywhere but prefers ceilings and sheer cliffs. Cook it before eating to temporarily increase your movement speed.
54	180	183	185	188	Razorshroom	Food	5	0.5	{4}	This mushroom is known for the natural slice in its cap. Eating it fosters your competitive spirit. Use it when cooking to prepare a dish that will increase your strength.
55	181	184	186	189	Ironshroom	Food	5	0.5	{6}	The cap of this mushroom is very hard. Use it when cooking to prepare a dish that increases your defense.
56	182	185	187	190	Silent Shroom	Food	3	0.5	{2,30}	A strange mushroom that glows quietly in the forest at night. Cooking it into a dish unlocks the nutrients in its cap, resulting in a meal that will allow you to move stealthily.
57	183	186	188	191	Hyrule Herb	Plants	3	1	\N	This healthy herb grows abundantly in the plains of Hyrule. Cook it before eating to increase the number of hearts it restores.
58	184	187	189	192	Hearty Radish	Food	8	2.5	{26}	A rare radish that grows best in sunny plains. Cook it before eating to temporarily increase your maximum hearts.
59	185	188	190	193	Big Hearty Radish	Food	15	4	{26}	This hearty radish has grown much larger than the average radish. It's rich in analeptic compounds that, when cooked into a dish, temporarily increase your maximum hearts.
60	186	189	191	194	Cool Safflina	Plants	3	\N	{7}	This medicinal plant grows in high elevations, such as mountains in the Hebra or Gerudo regions. When cooked into a dish, it will temporarily increase your heat resistance.
61	187	190	192	195	Warm Safflina	Plants	3	\N	{2,5}	This medicinal plant grows in hot regions, such as the Gerudo Desert. It's warm to the touch and increases your cold resistance when cooked into a dish.
62	188	191	193	196	Electric Safflina	Plants	3	\N	{28}	This medicinal plant grows abundantly in the Gerudo Desert. Its peculiar fibers conduct electricity, which will increase your electricity resistance when cooked into a dish.
63	189	192	194	197	Swift Carrot	Food	4	0.5	{29}	This carrot is cultivated extensively in Kakariko Village. It strengthens the legs and hips when cooked into a dish, which helps increase your movement speed.
64	190	193	195	198	Endura Carrot	Plants	30	2	{23,27}	Highly valued as a medicinal plant, this carrot contains large amounts of nourishing energy. When cooked into a dish, it boosts your stamina beyond its maximum limit.
65	191	194	196	199	Fortified Pumpkin	Plants	5	0.5	{6}	An extremely tough pumpkin raised in village fields. When cooked, that toughness manifests itself by considerably upping defense.
66	192	195	197	200	Swift Violet	Plants	10	\N	{2,29}	This vitality-rich flower blooms mainly on cliffsides. When cooked into a dish, the nourishing compounds increase your movement speed.
67	193	196	198	201	Mighty Thistle	Plants	5	\N	{4}	This medicinal plant is known for its sharp thorns and for the fruit it bears. The fruit contains a compound that increases attack power when cooked into a dish.
68	194	197	199	202	Armoranth	Plants	5	\N	{6}	This tough medicinal plant cannot be broken, but it can be cooked. Its durable yet flexible fibers raise your defense when cooked into a dish.
69	195	198	200	203	Blue Nightshade	Plants	4	\N	{2,30}	A plant that grows in quieter areas of Hyrule. At night, it gives off a soft glow. Cook with it to increase your stealth.
70	196	199	201	204	Silent Princess	Plants	10	\N	{2,30}	This lovely flower was said to have been a favorite of the princess of Hyrule. Once feared to have gone extinct, it's recently been spotted growing in the wild.
71	197	200	202	205	Courser Bee Honey	Food	10	2	{2,32}	Honey straight from the hive is chock-full of nutrients. Cooking this into a meal unlocks the potential of these nutrients and provides a stamina-recovery effect.
\.


--
-- Data for Name: materials_additional_uses; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.materials_additional_uses (id, additional_use) FROM stdin;
1	Armor Crafting
2	Armor Upgrades
3	Automatically used upon death
4	Boosts attack power when cooked
5	Boosts cold resistance when cooked
6	Boosts defense when cooked
7	Boosts heat resistance when cooked
8	Boosts shock resistance when cooked
9	Campfires
10	Can be created by heating Chuchu jelly over an open flame or killing any ChuChu with a flame weapon
11	Can be created by killing any Chuchu with an electric weapon
12	Can be created by killing any ChuChu with an Ice weapon
13	Can make elixirs that boost defense
14	Can make elixirs that increase max hearts
15	Can make elixirs that increase speed
16	Can make elixirs that raise attack power
17	Can make elixirs that raise heat resistance
18	Can make elixirs that raise shock resistance
19	Can make elixirs that restore stamina
20	Can make elixirs which increase stealth
21	Champion weapon crafting
22	Elixir Creation
23	Fed to horses to increase max horse stamina
24	Fire starting
25	General cooking and specific cooking recipes
26	Increases max hearts when cooked
27	Increases max stamina when cooked
28	Increases shock resistance when cooked
29	Increases speed when cooked
30	Increases stealth when cooked
31	Restores or removes a random number of hearts
32	Restores stamina when cooked
33	Sold for Mon
34	Sold for Rupees
35	Trading for Diamonds
36	Can make elixirs which boost cold resistance
37	Can make elixirs which increase max stamina
\.


--
-- Data for Name: monsters; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.monsters (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, monster_type, health, base_damage, recoverable_materials, description) FROM stdin;
1	84	84	84	84	Chuchu	chuchu	3	2	{84}	This low-level, gel-based monster can be found all over Hyrule. It tends to spring its attacks on unsuspecting prey from the ground or from trees. Its strength varies by size, and the type of jelly it drops varies depending on whether the Chuchu was heated up, cooled down, or shocked.
2	85	85	85	85	Fire Chuchu	chuchu	3	6	{130}	This low-level gel monster is engulfed in flames. Its strength varies depending on its size. It tends to explode if attacked from close range, so the use of spears, arrows, and other ranged weapons is advised.
3	86	86	86	86	Ice Chuchu	chuchu	3	3	{145}	This low-level gel monster is engulfed in freezing-cold air. Its strength varies depending on its size. It tends to explode if attacked from close range, so the use of spears, arrows, and other ranged weapons is advised
4	87	87	87	87	Electric Chuchu	chuchu	3	9	{147}	This low-level gel monster is engulfed in electricity. Its strength varies depending on its size. It tends to explode if attacked from close range, so the use of spears, arrows, and other range weapons is advised.
5	88	88	88	88	Keese	keese	1	4	{103,104}	The unpredictable flight pattern of this nocturnal bat-like species can make fighting them a nuisance, but they're weak enough to fell with a single strike. Sometimes they'll attack in packs, but even then, a pack can be sent packing with a single attack.
6	89	89	89	89	Fire Keese	keese	1	10	{91,103}	The fire that engulfs the bodies of these bat-like Keese makes them more dangerous than the standard type. They're capable of setting fire to anything they touch.
7	90	90	90	90	Ice Keese	keese	1	6	{101,103}	The intense frost that engulfs the bodies of these bat-like Keese makes them more dangerous than the standard type. They're capable of freezing anything they touch.
8	91	91	91	91	Electric Keese	keese	1	8	{88,103}	The electricity that engulfs the bodies of these bat-like Keese makes them more dangerous than the standard type. They're capable of shocking anything they touch.
9	92	92	92	92	Water Octorok	octorok	8	2	{120,121,122}	Although they spend most of their time in water, the drop in barometric pressure that occurs when it rains causes an air sac within these octopus-like monsters to inflate and lift them into the air. the rocks they spit out can be bounced back with a shield.
10	93	93	93	93	Forest Octorok	octorok	8	2	{120,121,122}	Although originally an aquatic species, this type has adapted to life in the forest. They hide among the trees, disguising themselves as grass or unassuming shrubbery, and then attack when someone walks by.
11	94	94	94	94	Rock Octorok	octorok	8	1	{120,121,122}	This octopus-like species of monster lives in volcanic regions. When they inhale, they're preparing to spit out flaming rocks but have been known to suck up weapons or bombs in the same breath.
12	95	95	95	95	Snow Octorok	octorok	8	2	{120,121,122}	These octopus-like monsters live in snowy fields and disguise themselves as grass. When someone wanders by, they spring into action and attack by spitting snowballs.
13	96	96	96	96	Treasure Octorok	octorok	8	3	{120,121,122}	These particularly clever monsters bury themselves in deep sand or snow and disguise themselves as treasure chests. Anyone who approaches the chests is attacked. The treasure chests are not magnetic, which proves that they are actually a part of these monster's bodies.
14	97	97	98	98	Fire Wizzrobe	wizzrobe	150	10	\N	These spell-casting monsters can be found all over Hyrule. They use their fire rods to hurl fireballs or to summon flaming monsters and have been known to severely raise the temperature around them. The weather will normalize once the Wizzrobe is defeated.
15	98	98	99	99	Ice Wizzrobe	wizzrobe	150	10	\N	These spell-casting monsters can be found all over Hyrule. They use their ice rods to create freezing blasts of air or to summon frozen monsters and have been known to cause blizzards to severely decrease the temperature around them. The weather will normalize once the Wizzrobe is defeated.
16	99	99	100	100	Electric Wizzrobe	wizzrobe	150	10	\N	These spell-casting monsters can be found all over Hyrule. They use their lightning rods to hurl balls of electricity or to summon monsters surging with electricity and have been known to cause thunderstorms in the area. The weather will normalize once the Wizzrobe is defeated.
17	100	100	101	101	Meteo Wizzrobe	wizzrobe	300	20	\N	These spell-casting monsters can be found all over Hyrule. They use their meteor rods to hurl fireballs or to summon flaming monsters and have been known to severely raise the temperature around them. The weather will normalize once the Wizzrobe is defeated.
18	101	101	102	102	Blizzrobe	wizzrobe	300	30	\N	These spell-casting monsters can be found all over Hyrule. They use their blizzard rods to create freezing blasts of air or to summon frozen monsters and have been known to cause blizzards to severely decrease the temperature around them. The weather will normalize once the Wizzrobe is defeated.
19	102	102	103	103	Thunder Wizzrobe	wizzrobe	300	30	\N	These spell-casting monsters can be found all over Hyrule. They use their thunderstorm rods to hurl balls of electricity or to summon monsters surging with electricity and have been known to cause thunderstorms in the area. The weather will normalize once the Wizzrobe is defeated.
20	103	103	104	104	Bokoblin	bokoblin	13	1	{80,154}	This common species is a nuisance all over Hyrule. Some have unified in the time following the Great Calamity and have formed factions of bandits. While not very clever, they are at least intelligent enough to hunt beasts and grill the meat for food. Though they're typically ferocious carnivores, they actually enjoy fruit as well.
21	104	104	105	105	Blue Bokoblin	bokoblin	72	4	{80,81,154}	This common species is a nuisance all over Hyrule. They're tougher and have stronger weapons than the red Bokoblins--and are a little more clever, as well. At the very least, they figured out that they can simply kick a Remote Bomb out of the way to avoid its blast.
22	105	105	106	106	Black Bokoblin	bokoblin	240	8	{80,81,154}	Although Bokoblins are generally a nuisance, the Black Bokoblins are among the most dangerous type. They're extremely resilient and many are armed with advanced weapons.
23	106	106	107	107	Stalkoblin	bokoblin	1	1	{80,154}	The remains of Bokoblins appear in the dark of the night. While they're fragile enough to crumble from a single blow, as long as a skull remains intact, they'll continue to pull themselves back together and go on fighting. Sometimes the body will pick up the wrong skull, but this doesn't seem to be a problem...
24	107	107	108	108	Silver Bokoblin	bokoblin	720	24	{80,81,154,73,85,123,133,134,144}	You would be foolish to call these Silver Bokoblins a mere nuisance. They have been influenced by Ganon's fiendish magic, so they are stronger than even the Black Bokoblins. They're called silver not only because of their coloring but also to denote their rarity. The purple markings help them stand out even more.
25	108	108	110	110	Moblin	moblin	56	2	{112,114}	This heavyweight species of monster can be found all over Hyrule. They're physically very strong, their legs alone strong enough to resist the force of a bomb blast. They're much more dangerous than Bokoblins. In fact, Moblins have been know to pick up Bokoblins and throw them as makeshift projectile weapons.
26	109	109	111	111	Blue Moblin	moblin	144	10	{112,113,114}	These heavyweight monsters can be found all over Hyrule. They're much tougher than their standard counterparts and often have much stronger weapons equipped.
27	110	110	112	112	Black Moblin	moblin	360	20	{112,113,114}	These heavyweight monsters can be found all over Hyrule and are among the most dangerous types of Moblin. They're extremely resilient and are often armed with some of the strongest weapons Moblins can carry.
28	111	111	113	113	Stalmoblin	moblin	2	10	{112,114}	The remains of Moblins appear in the dark of the night. Even the toughest Moblins become fragile when they're little more than a pile of bones, so they'll crumble after just a few attacks. However, as long as a skull remains intact, they'll continue to pull themselves back together and go on fighting.
29	112	112	114	114	Silver Moblin	moblin	1080	24	{112,113,114,85,123,133,134,144}	The strongest of all Moblins, Ganon's fiendish magic has allowed them to surpass even the Black Moblins in strength and resilience. They're called silver for both their body color as well as their rarity. The purple patterns on their bodies also help them stand out.
30	113	113	116	116	Lizalfos	lizalfo	50	4	{105,106}	These quick-witted, lizard-like monsters can be found all over Hyrule. They're a sly species that lurks underwater or uses camouflage to blend in with the environment to launch ambushes. Moreover, they never sleep. They're meat eaters by nature but will enjoy the occasional insect or two.
31	114	114	117	117	Blue Lizalfos	lizalfo	120	8	{105,106,107}	These quick-witted, lizard-like monsters can be found all over Hyrule. Compared to the green Lizalfos, many of these carry much stronger weapons and are generally much tougher.
32	115	115	118	118	Black Lizalfos	lizalfo	288	16	{105,106,107}	These quick-witted, lizard-like monsters can be found all over Hyrule. This particular type tends to carry some pretty strong weapons, so they are among the most dangerous Lizalfos.
33	116	116	119	119	Stalizalfos	lizalfo	1	10	{105,106}	The remains of Lizalfos appear in the dark of the night. They're as sly as ever, even now that they're just some bones stacked atop each other. However, their bodies are much more fragile, and a single solid hit can reduce them to pieces. If skull remains intact, they will pull themselves back up and continue to fight. They've been known to grab the wrong skull at times, but they never seem to mind...
34	117	117	120	120	Fire-Breath Lizalfos	lizalfo	160	16	{105,106,131}	These quick-witted, lizard-like monsters can be found all over Hyrule. Their fiery breath makes them pretty dangerous, but exposure to cold will kill them instantly. They mainly make their homes in volcanic areas but have also been sighted in the Akkala region.
35	118	118	121	121	Ice-Breath Lizalfos	lizalfo	288	16	{105,106,102}	These quick-witted, lizard-like monsters can be found all over Hyrule. The balls of ice they spit make them particularly troublesome, but exposure to fire will kill them instantly. Their homes are mainly on snowy mountains.
36	119	119	122	122	Electric Lizalfos	lizalfo	288	16	{105,106,148}	These quick-witted, lizard-like monsters can be found all over Hyrule. They can emit strong electrical currents from their bodies, so don't get too close. Their horns are brimming with electricity, which will discharge and arc to nearby areas if stuck by an arrow. They tend to like in desert climates.
37	120	120	123	123	Silver Lizalfos	lizalfo	864	20	{105,106,107,73,85,123,133,134,144}	These Lizalfos have been influenced by Ganon's fiendish magic to become the strongest Lizalfos of all. They're called silver for their unique coloring and also to denote their rarity. Their purple pattern makes them even more distinct.
38	121	121	125	125	Lynel	lynel	2000	15	{109,110,111}	These fearsome monsters have lived in Hyrule since ancient times. They possess intense intelligence, resilience, and strength, making them among the most dangerous monsters in all the land. This is compounded by the fact that they have a natural resistance to all elements. you would be wise to challenge a Lynel only if you're very well prepared.
39	122	122	126	126	Blue-Maned Lynel	lynel	3000	20	{109,110,111}	These fearsome monsters have lived in Hyrule since ancient times. Compared to the standard Lynel, those with blue manes are much tougher and are equipped with much stronger weapons. Facing off a Lynel is ill-advised, but if you must, be sure you're well prepared.
40	123	123	127	127	White-Maned Lynel	lynel	4000	26	{109,110,111}	These fearsome monsters have lived in Hyrule since ancient times. Their ability to breathe fire makes White-Maned Lynels among the toughest of the species; each one of their attacks is an invitation to the grave. There are so few eyewitness accounts of this breed because a White-Maned Lynel is not one to let even simple passersby escape with their lives.
41	124	124	128	128	Silver Lynel	lynel	5000	30	{109,110,111,73,85,123,133,134,141,144}	Silver Lynels are not to be trifled with. They have been influenced by Ganon's fiendish magic, so they are the strongest among the Lynel species, surpassing even the strength of those with white manes. The term silver denotes not only their color but also their rarity. The purple stripes help them to stand out even more.
42	125	125	130	130	Guardian Stalker	guardian	1500	58	{74,75,76,77,78,94}	The Sheikah of ancient Hyrule developed this as a weapon to combat Ganon. Its six legs give it extraordinary mobility compared to most current vehicles, and its powerful laser provides far greater offensive capability than conventional weaponry. Destroying the legs severely reduces its mobility.
43	126	126	131	131	Guardian Skywatcher	guardian	1500	58	{74,75,76,77,78,94}	The Sheikah of ancient Hyrule developed this as a weapon to combat Ganon. This flying model is an improbement over the walking type, capable of either attacking or surveying from the air. Destroying the propeller will ground it.
44	127	127	132	132	Guardian Turret	guardian	1500	58	{74,75,76,77,78,94}	The Sheikah of ancient Hyrule developed this as a weapon to combat Ganon. This stationary model was used for defending important structures. Its offensive power is on par with other Guardians, and omitting the legs kept the manufacturing costs low.
45	128	128	133	133	Sentry	guardian	1000	\N	{74,75,76,77,78}	These sentries dispatched from Divine Beast Vah Rudania are equipped with searchlights that can spot intruders.
46	129	129	134	134	Decayed Guardian	guardian	500	58	{75,76,77,78}	The Sheikah of ancient Hyrule developed this as a weapon to combat Ganon, but it was destroyed during the Great Calamity. Ages of weather and neglect have left it in a state of disrepair. Approach it with caution; some of these derelict models have been known to awaken from stasis and attack when approached.
47	130	130	135	135	Guardian Scout I	guardian	13	1	{76,77}	Guardians were originally designed by an ancient civilization to combat Ganon, but these smaller models were placed inside shrines as part of the trials found within. The multiple legs and beam functionality were scaled down but kept mostly intact.
48	131	131	136	136	Guardian Scout II	guardian	375	10	{75,76,77,78}	Although originally designed by an ancient civilization to combat Ganon, these scaled-down guardians were placed inside shrines as part of the trials. In addition to the beam functionality, this particular model was designed to handle weaponry as a means of further testing the combat prowess of one undertaking the trials. It takes some serious skill to go toe-to-toe with one of these.
49	132	132	137	137	Guardian Scout III	guardian	1500	15	{74,75,76,77,78}	Although originally designed by an ancient civilization to combat Ganon, these scaled-down Guardians were placed inside shrines as part of the trials. This model is equipped with twin-blade functionality to further test the combat prowess of one undertaking the trials. It takes a nimble fighter to overcome this one.
50	133	133	138	138	Guardian Scout IV	guardian	3000	20	{74,75,76,77,78}	An ancient civilization originally designed Guardians to combat Ganon but then scaled them down to place in shrines as part of the trials. This model is very resilient and has been outfitted with triple-blade functionality, allowing it to wield three weapons. This will put any would-be hero to the test for sure. A great deal of strength and confidence alike are required to contend with one of these.
51	134	134	139	139	Yiga Footsoldier	yiga	64	8	{44,149,150,151,153}	The lowest-rank members of the Yiga Clan. They've been dispatched all across Hyrule with a single task: seek out link and end his life. They're a crafty bunch, sometimes disguising themselves as simple travelers or villagers to get the jump on you. Be wary of suspicious people you encounter. They're very agile and carry a bow and a one- handed sword.
52	135	135	140	140	Yiga Blademaster	yiga	400	10	{44,73,123,133,134,144}	These are the elite soldiers of the Yiga Clan. They favor the windcleaver blade and are extremely agile despite their bulky build. At this Yiga Clan rank, they have mastered a technique that allows them to manipulate the very earth. By striking the ground, they can create stone pillars and blasts of air.
53	136	136	141	141	Master Kohga	yiga	300	\N	\N	The leader of the Yiga Clan, a group formed with just a single objective:eliminate Link. He sends his minions all over Hyrule in search of you but tends to spend most of his own time napping and generally loafing about. Despite this, his mastery of the esoteric arts has earned him a deep respect. Even if he were to die, his followers would never give up their one and only task.
54	137	138	142	143	Stone Talus	talus	300	60	{73,92,123,133}	As the last part of the final trial, the monk offers a challenge of ancient techniques.
55	138	139	143	144	Stone Talus (Luminous)	talus	600	60	{73,92,123,144,108,85}	This enormous monster is naturally camouflaged as a rock formation. Neither sword nor arrow can pierce its stony form, but a cunning adventurer knows to scale its body and attack the ore sprouting from its peak. Unlike your average Stone Talus, this type's ore deposit consists mostly of luminous stone.
56	139	140	144	145	Stone Talus (Rare)	talus	900	60	{73,85,92,123,133,134,144}	This enormous monster is naturally camouflaged as a rock formation. Neither sword nor arrow can pierce its stony form, but a cunning adventurer knows to scale its body and attack the ore sprouting from its peak. Unlike your average Stone Talus, this type's ore deposit contains a large amount of precious ore.
57	140	141	145	146	Igneo Talus	talus	800	66	{85,92,123,133}	This enormous monster is naturally camouflaged as molten rock. Neither sword nor arrow can pierce its fiery form. Merely touching its magma-hot body can burn you pretty badly, but you may be able to scale it if you can use something to chill its flames.
58	141	142	146	147	Frost Talus	talus	800	66	{85,92,123,134}	This enormous monster is naturally camouflaged as a frozen rock formation. Neither sword nor arrow can pierce its frigid form. Merely touching its frosty body can leave you with severe frostbite, but you may be able to scale it if you use something to thaw its icy exterior.
59	142	143	147	148	Stone Pebblit	talus	20	16	{92,73,123}	A very young Stone Talus. Their bodies toughen as they mature, becoming as tough as boulders by adulthood. As a child, however, their bodies are light enough to be lifted and fragile enough to break when thrown.
60	143	144	148	149	Igneo Pebblit	talus	20	22	{92,73,133}	A very young Igneo Talus. Their bodies toughen as they mature and convert fully into lava by adulthood. As a child, however, while still covered in fire, their bodies are fragile and light enough to be blown away by a bomb's blast.
61	144	145	149	150	Frost Pebblit	talus	20	34	{92,73,134}	A very young Frost Talus. Their bodies toughen as they mature, becoming entirely made of ice by adulthood. As a child, however, their bodies are awfully fragile and are light enough to be blown away by a bomb's blast.
62	145	147	150	152	Hinox	hinox	600	24	{97,98,99,36,37,38,39,42,44,65}	The largest monster to make its home in Hyrule, the Hinox lives primarily in forested areas. A keen awareness of your surroundings is paramount when facing one, as Hinox are know for tearing entire trees from the ground and using them as weapons. A deft hand can steal weapons off the necklaces they wear.
84	\N	152	\N	157	Molduking	molduga	\N	100	\N	This massive monster swims beneath the desert's sand. It is a subspecies of Molduga that stored up a great deal of energy by sleeping underground for hundreds of years. Its power is superior to Molduga, as its skin is rich with iron and acts as a protective shield.
63	146	148	151	153	Blue Hinox	hinox	800	48	{97,98,99,157,161,162,163,164,169,155,156}	The largest monster to make its home in Hyrule, the Hinox lives primarily in forested areas. A keen awareness of your surroundings is paramount when facing one, as Hinox are know for tearing entire trees from the ground and using them as weapons. Blue Hinox wear greaves that can be burnt away to expose their feet. A deft hand can steal weapons off the necklaces they wear.
64	147	149	152	154	Black Hinox	hinox	1000	72	{97,98,99,158,159,165,166,167,168}	The largest monster to make its home in Hyrule, the Hinox lives primarily in forested areas. A keen awareness of your surroundings is paramount when facing one, as Hinox are know for tearing entire trees from the ground and using them as weapons. Black Hinox wear metal greaves that conduct electricity rather well. A deft hand can steal weapons off the necklaces they wear.
65	148	150	153	155	Stalnox	hinox	1000	60	{99}	The mere remains of what was once a giant monster known as a Hinox. These skeletal beasts appear at nighttime. Much like their living counterparts, the Stalnox will tear entire trees from the ground to use as weapons. Furthermore, even if it appears defeated at first, it will keep coming back for more as long as its eye is left intact.
66	149	151	154	156	Molduga	molduga	1500	60	{115,116}	This massive monster swims beneath the desert's sand. It spends most of its time submerged, but if it senses sound, it will breach the surface to feast on whatever it can grab. Running around carelessly can be dangerous if you suspect there may be one in the area.
67	150	153	155	158	Dinraal	dragon	\N	6	{86,87}	A spirit of fire has taken the form of this giant dragon. Making its home in the Eldin region, it's said to have served the Spring of Power since ancient times. An old saying goes, The dragon ascends to the heavens as the sun begins to set. but nobody has witnessed this in the current age. The flames that coat its body make is dangerous to get near, but Dinraal bears no ill will toward people.
68	151	154	156	159	Naydra	dragon	\N	3	{118,119}	A spirit of ice has taken the form of this giant dragon. Making its home in the Lanayru region, it's said to have served the Spring of Wisdom since ancient times. An old saying goes, The dragon ascends to the heavens as the sun begins to set, but nobody has seen this in the current age. The ice that coats its body makes it dangerous to get near, but Naydra bears no ill will toward people.
69	152	155	157	160	Farosh	dragon	\N	12	{89,90}	A spirit of lightning has taken the form of this giant dragon. Making its home in the Faron region, it's said to have served the Spring of Courage since ancient times. An old saying goes, The dragon ascends to the heavens as the sun begins to set, but nobody has seen this in the current age. The electricity that coats its body makes it dangerous to get near, but Farosh bears no ill will toward people.
70	153	156	158	161	Cursed Bokoblin	bokoblin	1	14	\N	The Malice has given these Bokoblin skulls a sort of life. Devoid of any intelligence the Bokoblin it once belonged to may have had, these pitiful things now exist only to attack anything that gets too close.
71	154	157	159	162	Cursed Moblin	moblin	30	16	\N	The Malice has given these Moblin skulls a pitiful life after death. The only fragment of their former selves they've held on to is the ferocity innate to all Moblins, so the will attack anyone that approaches. They're tougher than Bokoblin skulls.
72	155	158	160	163	Cursed Lizalfos	lizalfo	1	12	\N	The Malice has given these Lizalfos skulls a sorry form of life after death. Only the slyness of their former selves remains, making them faster than Bokoblin skulls. They thoughtlessly attack anyone who approaches.
73	156	159	161	164	Thunderblight Ganon	ganon	800	51	\N	This phantom of Ganon attacked the Divine Beast Vah Naboris and was responsible for the demise of the Champion Urbosa. It specializes in quick, lightning-based attacks.
74	157	160	162	165	Fireblight Ganon	ganon	800	45	\N	This phantom of Ganon attacked the Divine Beast Vah Rudania and was responsible for the demise of the Champion Daruk. It attacks with a greatsword and fire magic.
75	158	161	163	166	Waterblight Ganon	ganon	800	45	\N	This phantom of Ganon attacked the Divine Beast Vah Ruta and was responsible for the demise of the Champion Mipha. It attacks with a spear and ice magic.
76	159	162	164	167	Windblight Ganon	ganon	800	\N	\N	This phantom of Ganon attacked the Divine Beast Vah Medoh and was responsible for the demise of the Champion Revali. It specializes in long-range wind attacks.
77	160	163	165	168	Calamity Ganon	ganon	8000	\N	\N	The source of the darkness that has appeared time and again throughout Hyrule's history. It's been called many names, from Great King of Evil to Calamity. Hibernating within a cocoon, it attempted to regenerate a physical form after Link awoke but was forced to confront him in an incomplete state.
78	161	164	166	169	Dark Beast Ganon	ganon	8	72	\N	After Ganon was defeated by Link, the remaining Malice pulled itself together to form this bestial creature. Its appearance and fiendish magic earned it the name of Dark Beast. This form is considered to be Ganon's original, although in this state, his awareness has been consumed entirely by Malice, and all he knows is a desire to rampage and destroy.
79	\N	\N	109	109	Golden Bokoblin	bokoblin	1080	48	\N	With brutal strength and extreme resilience, this type of Bokoblin somehow surpasses Silver Bokoblins in sheer power. It is said they are actually Silver Bokoblins who mysteriously transformed after being struck by lightning. Perhaps that is why they have such a high tolerance for electric attacks.
80	\N	\N	124	124	Golden Lizalfos	lizalfo	1296	40	\N	With brutal strength and extreme resilience, this type of Lizalfos somehow surpasses Silver Lizalfos in sheer power. It is said they are actually Silver Lizalfos who mysteriously transformed after being struck by lightning. Perhaps that is why they have such a high tolerance for electric attacks.
81	\N	\N	129	129	Golden Lynel	lynel	7000	60	\N	With brutal strength and extreme resilience, this type of Lynel somehow surpasses Silver Lynels in sheer power. It is said they are actually Silver Lynels who mysteriously transformed after being struck by lightning. If you see one, get away as fast as you can.
82	\N	\N	115	115	Golden Moblin	moblin	1620	48	\N	With brutal strength and extreme resilience, this type of Moblin somehow surpasses Silver Moblins in sheer power. It is said they are actually Silver Moblins who mysteriously transformed after being struck by lightning. Perhaps that is why they have such a high tolerance for electric attacks.
83	\N	146	\N	151	Igneo Talus Titan	talus	\N	\N	\N	This monster is an Igneo Talus subspecies that is camouflaged as molten rock. It lives in lava for many years before emerging at an enormous size. It is wildly powerful and emits a tremendous amount of heat, causing a constant updraft in its vicinity. This monster is so fearsome it has earned the title of Titan.
85	\N	137	\N	142	Monk Maz Koshia	sheikah	\N	40	\N	The arbiter of worthiness for the hero who wishes to control a Divine Beast, following a revelation from the Goddess Hylia. As the last part of the final trial, the monk offers a challenge of ancient techniques.
86	\N	\N	97	97	Sky Octorok	octorok	\N	\N	\N	These low-level, octopus-like monsters used to be aquatic, but their inflatable air sacs evolved so that they are able to float around in the air. Other monsters are known to take advantage of this ability, using them to make floating fortresses. They are trained to ascend when they hear a whistle but they will not attack.
\.


--
-- Data for Name: recoverable_materials; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.recoverable_materials (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, material_type, value, restores, additional_uses, description) FROM stdin;
1	48	48	48	48	Hyrule Bass	Food	6	1	{2}	An ordinary fish that can be found all over Hyrule. Can be eaten raw, but cooking it amplifies its healing benefits.
2	49	49	49	49	Hearty Bass	Food	18	2	{2,26}	This large fish lives near the shoreline. Its sizable body can restore a lot of nutrient. When cooked into a dish, it will temporarily increase your maximum hearts.
3	50	50	50	50	Staminoka Bass	Food	18	1	{32}	This Hyrule bass got to be the biggest fish by never getting caught (until now). Its long life results in a cooked dish that will restore a lot of stamina.
4	51	51	51	51	Hearty Salmon	Food	10	4	{26}	This fish makes its home in cold water, giving it extra layers of fat. It temporarily increases your maximum hearts when used in cooking.
5	52	52	52	52	Chillfin Trout	Food	6	1	{7}	This blue trout prefers cold bodies of water. Its skin contains enzymes that keep its body cool, and when cooked into a dish, it will temporarily boost your heat resistance.
6	53	53	53	53	Sizzlefin Trout	Food	6	1	{5}	This red trout prefers warm bodies of water. It has a special organ specifically for storing heat. When cooked into a dish, it temporarily boosts your resistance to the cold.
7	54	54	54	54	Voltfin Trout	Food	6	1	{8}	This trout makes its home in the freshwater lakes. Its scales have an insulating compound that, when cooked into a dish, offers resistance to electricity.
8	56	56	56	56	Mighty Carp	Food	10	1	{4}	This freshwater fish lives alongside its less mighty carp ilk. A compound in its liver promotes muscle growth. Dishes cooked with it will temporarily increase your attack power.
9	57	57	57	57	Armored Carp	Food	10	1	{6}	Calcium deposits in the scales of this ancient fish make them as hard as armor. Cooking it into a dish will fortify your bones, temporarily increasing your defense.
10	58	58	58	58	Sanke Carp	Food	20	1	\N	This wild armored carp has been bred into a prizewinning fish. Its beautifully colored markings do not occur in nature. It offers no special effects when cooked.
11	59	59	59	59	Mighty Porgy	Food	10	1	{4}	This ocean-dwelling fish comes with one rude attitude. The compounds in its flesh elevate your competitive spirit when it's cooked into a dish, thus increasing your attack power.
12	60	60	60	60	Armored Porgy	Food	10	1	{6}	This porgy's body is covered in armor-hard scales. The compounds in its scales, when cooked into a dish, fortify your bones and temporarily boost your defense.
13	61	61	61	61	Sneaky River Snail	Critters	6	1	{2,30}	This large, glow-in-the-dark snail lives in fresh water. When cooked into a dish, it heightens your senses so you can move about silently.
14	62	62	62	62	Hearty Blueshell Snail	Food	15	3	{26}	This snail lives on sandy beaches in large numbers. Its flesh contains a high amount of stimulants, so when cooked into a dish, it temporarily increases your maximum hearts.
15	63	63	63	63	Razorclaw Crab	Food	8	1	{4}	This crab is well known for its exceptionally sharp pincers. When cooked, the strength compound in its claws will increase your attack power.
16	64	64	64	64	Ironshell Crab	Food	8	1	{6}	This crab's shell is particularly hard. When cooked into a dish, its fat and meat bolster the body to increase your defense.
17	65	65	65	65	Bright-Eyed Crab	Food	10	1	{32}	This crab appears in large numbers when it rains. On bite of its delectable meat, and you'll forget all your exhaustion. Replenishes your stamina when cooked into a dish.
18	66	66	66	66	Fairy	Other	2	5	{3}	This fairy will fly from your pouch and heal all your wounds the moment you lose your last heart. It's easily mistaken for a firefly at first, but it glows in the daylight as well as night.
19	67	67	67	67	Winterwing Butterfly	Critters	2	\N	{7}	The powdery scales of this butterfly's wings cool the air around it. Watching it flutter around snowflakes is a thing of beauty. Cook it with monster parts for a heat-resistant elixir.
20	68	68	68	68	Summerwing Butterfly	Critters	2	\N	{36}	A butterfly found in the woods and plains of warm regions. Its wings absorb the warmth of the sun. Cook it with monster parts to create an elixir that makes you feel warm and fuzzy.
21	69	69	69	69	Thunderwing Butterfly	Critters	2	\N	{18}	This rare butterfly only shows itself when it rains. The organs in its body produce an insulating compound. When made into an elixir, it offers electrical resistance.
22	70	70	70	70	Smotherwing Butterfly	Critters	2	\N	{2,17}	This rare butterfly lives in volcanic regions. Its body contains a heat-resistant liquid, which can be turned into a topical elixir that offers resistance to flames.
23	71	71	71	71	Cold Darner	Critters	2	\N	{17}	This dragonfly prefers the cool shade of trees to the warmth of the sun. Its wings disperse heat from its body, which can be cooked into a heat-resistant elixir.
24	72	72	72	72	Warm Darner	Critters	2	\N	{36}	This dragonfly has a special organ that causes it to sweat profusely. Cook it with monster parts for an elixir that will raise your core temperatures so you can resist the cold.
25	73	73	73	73	Electric Darner	Critters	2	\N	{18}	This rare dragonfly only appears in the rain. Its wings direct electricity away from its body. Cook it with monster parts for an electricity-resistant elixir.
26	74	74	74	74	Restless Cricket	Critters	2	\N	{19}	A very energetic cricket. Cook it with monster parts to create a stamina-recovery elixir.
27	75	75	75	75	Bladed Rhino Beetle	Critters	4	\N	{16}	This beetle's razor-sharp horns demand that you handle it with care. Boil the horns alongside monster parts to concoct an elixir that will raise your attack power.
28	76	76	76	76	Rugged Rhino Beetle	Critters	4	\N	{13}	This beetle's hard body resembles armor. When the shell is cooked with monster parts, the resulting elixir boosts your defense.
29	77	77	77	77	Energetic Rhino Beetle	Critters	30	\N	{2,19}	This valuable beetle can live up to ten years. When cooked with monster parts, its impressive vitality translates into an elixir that will greatly restore your stamina.
161	\N	\N	\N	\N	Roasted Hearty Bass	Dish	\N	\N	\N	\N
162	\N	\N	\N	\N	Roasted Hearty Salmon	Dish	\N	\N	\N	\N
163	\N	\N	\N	\N	Roasted Porgy	Dish	\N	\N	\N	\N
30	78	78	78	78	Sunset Firefly	Critters	2	\N	{2,20}	These fireflies glow gently in the dark. When cooked with monster parts, the compound that causes it to glow results in an elixir that will allow you to move more quietly.
31	79	79	79	79	Hot-Footed Frog	Critters	2	\N	{2,15}	A quick frog that can be found hopping around near water. Cook it with monster parts to draw out its speed-boost effect.
32	80	80	80	80	Tireless Frog	Critters	20	\N	{37}	This rare frog only ventures out in the rain. When cooked with monster parts, the elixir it produces will temporarily increase your maximum stamina.
33	81	81	81	81	Hightail Lizard	Critters	2	\N	{2,15}	A lizard found throughout Hyrule. It's a bit slow to react at times, but if given a chance to escape, it will dart off quickly. Cook it with monster parts for a speed-boosting elixir.
34	82	82	82	82	Hearty Lizard	Critters	20	\N	{14}	This rare lizard lives deep in the forests. It feeds on high-nutrient foods, giving it great vitality. When used to make elixirs, they temporarily increase your maximum hearts.
35	83	83	83	83	Fireproof Lizard	Critters	5	\N	{2,17}	This rare lizard can only be found in the Eldin region. Its scales have heat-resistant properties, so when cooked with monster parts, it produces a heat-resistant elixir.
36	162	165	167	170	Apple	Food	3	0.5	\N	A common fruit found on trees all around Hyrule. Eat it fresh, or cook it to increase its effect.
37	163	166	168	171	Palm Fruit	Food	4	1	\N	Fruit from palm trees that grow near the ocean. It doesn't offer any special effects but will increase your heart recovery when used as an ingredient.
38	164	167	169	172	Wildberry	Food	3	0.5	\N	A fruit that grows in cold, snowy regions known for its tangy, sweet flavor. It doesn't offer any special effects, but it's a popular cooking ingredient.
39	165	168	170	173	Hearty Durian	Food	15	3	{26}	This fruit's odor earned it the nickname king of fruits. It offers immense restorative power; dishes cooked with it will temporarily increase your maximum hearts.
40	166	169	171	174	Hydromelon	Food	4	0.5	{7}	This resilient fruit can flourish even in the heat of the desert. The hydrating liquid inside provides a cooling effect that, when cooked, increases your heat resistance.
41	167	170	172	175	Spicy Pepper	Food	3	0.5	{7}	This pepper is exploding with spice. Cook with it to create dishes that will raise your body temperature and help you withstand the cold.
42	168	171	173	176	Voltfruit	Food	4	0.5	{2,8}	Cacti found in the Gerudo Desert bear this sweet fruit. It's naturally insulated, so when cooked into a dish, it provides resistance against electricity.
43	169	172	174	177	Fleet-Lotus Seeds	Food	5	0.5	{29}	The plant that bears these seeds grows near deep water. The roots draw nutrients from the water, which boosts your movement speed when the seeds are cooked into a dish.
44	170	173	175	178	Mighty Bananas	Food	5	0.5	{4}	This fruit grows mainly in tropical forests of the Faron region. When it's used as an ingredient, the resulting dish will temporarily increase your attack power.
45	171	174	176	179	Hylian Shroom	Food	3	0.5	\N	A common mushroom found near trees around Hyrule. Eat it to restore half a heart.
46	172	175	177	180	Endura Shroom	Food	6	1	{27}	A rare yellowish-orange mushroom. Cook it before eating to temporarily increase your stamina limit.
47	173	176	178	181	Stamella Shroom	Food	5	1	{32}	A green mushroom that grows near trees in the forest. It's chock-full of natural energy. Cook it to release its stamina-restoration properties.
48	174	177	179	182	Hearty Truffle	Food	6	2	{26}	This rare mushroom has a rich scent. Cook it before eating to temporarily increase your maximum hearts.
49	175	178	180	183	Big Hearty Truffle	Food	15	3	{26}	Years of going unpicked have allowed this hearty truffle to grow quite large. It's chock-full of nutrients. When cooked into a dish, it temporarily increases your maximum hearts.
50	176	179	181	184	Chillshroom	Food	4	0.5	{7}	Often found at the base of pine trees in cold climates, these mushrooms are cool to the touch and can be used to cook dishes that allow you to stay cool even in arid regions.
51	177	180	182	185	Sunshroom	Food	4	0.5	{2,5}	A bright red mushroom that grows in hot climates. Imbued with the power of heat, they can be used to cook dishes that will allow you to endure the bitter cold.
52	178	181	183	186	Zapshroom	Food	4	0.5	{2,8}	This mushroom grows wild in the Gerudo region. The cap is naturally insulated, so when used in cooking, it will offer protection against electricity.
53	179	182	184	187	Rushroom	Food	3	0.5	{2,29}	A mushroom that can grow almost anywhere but prefers ceilings and sheer cliffs. Cook it before eating to temporarily increase your movement speed.
54	180	183	185	188	Razorshroom	Food	5	0.5	{4}	This mushroom is known for the natural slice in its cap. Eating it fosters your competitive spirit. Use it when cooking to prepare a dish that will increase your strength.
55	181	184	186	189	Ironshroom	Food	5	0.5	{6}	The cap of this mushroom is very hard. Use it when cooking to prepare a dish that increases your defense.
56	182	185	187	190	Silent Shroom	Food	3	0.5	{2,30}	A strange mushroom that glows quietly in the forest at night. Cooking it into a dish unlocks the nutrients in its cap, resulting in a meal that will allow you to move stealthily.
57	183	186	188	191	Hyrule Herb	Plants	3	1	\N	This healthy herb grows abundantly in the plains of Hyrule. Cook it before eating to increase the number of hearts it restores.
58	184	187	189	192	Hearty Radish	Food	8	2.5	{26}	A rare radish that grows best in sunny plains. Cook it before eating to temporarily increase your maximum hearts.
59	185	188	190	193	Big Hearty Radish	Food	15	4	{26}	This hearty radish has grown much larger than the average radish. It's rich in analeptic compounds that, when cooked into a dish, temporarily increase your maximum hearts.
60	186	189	191	194	Cool Safflina	Plants	3	\N	{7}	This medicinal plant grows in high elevations, such as mountains in the Hebra or Gerudo regions. When cooked into a dish, it will temporarily increase your heat resistance.
164	\N	\N	\N	\N	Roasted Trout	Dish	\N	\N	\N	\N
165	\N	\N	\N	\N	Roasted Whole Bird	Dish	\N	\N	\N	\N
61	187	190	192	195	Warm Safflina	Plants	3	\N	{2,5}	This medicinal plant grows in hot regions, such as the Gerudo Desert. It's warm to the touch and increases your cold resistance when cooked into a dish.
62	188	191	193	196	Electric Safflina	Plants	3	\N	{28}	This medicinal plant grows abundantly in the Gerudo Desert. Its peculiar fibers conduct electricity, which will increase your electricity resistance when cooked into a dish.
63	189	192	194	197	Swift Carrot	Food	4	0.5	{29}	This carrot is cultivated extensively in Kakariko Village. It strengthens the legs and hips when cooked into a dish, which helps increase your movement speed.
64	190	193	195	198	Endura Carrot	Plants	30	2	{23,27}	Highly valued as a medicinal plant, this carrot contains large amounts of nourishing energy. When cooked into a dish, it boosts your stamina beyond its maximum limit.
65	191	194	196	199	Fortified Pumpkin	Plants	5	0.5	{6}	An extremely tough pumpkin raised in village fields. When cooked, that toughness manifests itself by considerably upping defense.
66	192	195	197	200	Swift Violet	Plants	10	\N	{2,29}	This vitality-rich flower blooms mainly on cliffsides. When cooked into a dish, the nourishing compounds increase your movement speed.
67	193	196	198	201	Mighty Thistle	Plants	5	\N	{4}	This medicinal plant is known for its sharp thorns and for the fruit it bears. The fruit contains a compound that increases attack power when cooked into a dish.
68	194	197	199	202	Armoranth	Plants	5	\N	{6}	This tough medicinal plant cannot be broken, but it can be cooked. Its durable yet flexible fibers raise your defense when cooked into a dish.
69	195	198	200	203	Blue Nightshade	Plants	4	\N	{2,30}	A plant that grows in quieter areas of Hyrule. At night, it gives off a soft glow. Cook with it to increase your stealth.
70	196	199	201	204	Silent Princess	Plants	10	\N	{2,30}	This lovely flower was said to have been a favorite of the princess of Hyrule. Once feared to have gone extinct, it's recently been spotted growing in the wild.
71	197	200	202	205	Courser Bee Honey	Food	10	2	{2,32}	Honey straight from the hive is chock-full of nutrients. Cooking this into a meal unlocks the potential of these nutrients and provides a stamina-recovery effect.
72	\N	\N	\N	\N	Acorn	Food	2	0.25	{2}	Often found on the ground near trees. Squirrels adore this nut, so you may have competition while foraging. Add one to a meal for a nutty seasoning.
73	\N	\N	\N	\N	Amber	Minerals	30	\N	{1,2,34}	A fossilized resin with a caramelesque sheen to it. It's been valued as a component in decorations and crafting since ancient times.
74	\N	\N	\N	\N	Ancient Core	Monster Parts	80	\N	{2,22,33,34}	This crystal was made using lost technology. At one time it was the power source for ancient machines. This item is very valuable to researchers.
75	\N	\N	\N	\N	Ancient Gear	Monster Parts	30	\N	{2,22,33,34}	A gear used in ancient machinery. Despite being incredibly old, its build quality is leaps and bounds above anything built using current technology.
76	\N	\N	\N	\N	Ancient Screw	Monster Parts	12	\N	{2,22,33,34}	A screw used in ancient machinery. It's made of an unknown material, and no matter how many times it's turned, its threads never seem to show signs of wear.
77	\N	\N	\N	\N	Ancient Shaft	Monster Parts	40	\N	{2,22,33,34}	A machine part used in ancient machinery. It's incredibly sturdy, and it's not made of any recognizable material. It may come in handy someday.
78	\N	\N	\N	\N	Ancient Spring	Monster Parts	15	\N	{2,22,33,34}	A spring used in ancient machinery. It is light and buoyant enough to float on water, and no matter how many times it's compressed, it never loses tension.
79	\N	\N	\N	\N	Bird Egg	Food	3	1	\N	A fresh bird egg necessary for making dishes such as omelets and crepes. You can snag them from birds' nests if you're sneaky. Nutritious and delicious, perfect.
80	\N	\N	\N	\N	Bokoblin Fang	Monster Parts	8	\N	{2,22,33,34}	A tooth obtained from a Bokoblin. It's worn down and not very sharp, but it's still pretty hard. Cook it alongside a critter to make an elixir.
81	\N	\N	\N	\N	Bokoblin Guts	Monster Parts	20	\N	{2,22,33,34}	A rare material obtained by defeating a Bokoblin. It convulses on its own every now and then, which is really creepy, but perhaps it has a use.
82	\N	\N	\N	\N	Cane Sugar	Plants	3	\N	{25}	When boiled with other ingredients, the cane breaks down into a sweet juice necessary for making cakes and other sweets. It's commonly found in ingredients stores.
83	\N	\N	\N	\N	Chickaloo Tree Nut	Food	3	0.25	\N	Small birds love this nut. You can eat it raw for a minor effect, but it can also be added as a spice to other recipes.
84	\N	\N	\N	\N	Chuchu Jelly	Monster Parts	5	\N	{2,22,33,34}	A gelatinous substance that came from a Chuchu. It's unusable in this state, but applying a bit of elemental stimulation will change its form.
85	\N	\N	\N	\N	Diamond	Minerals	500	\N	{1,21,34}	The most precious gem one can find in Hyrule, its signature sparkle has charmed Hyruleans for generations. As such, it has sold for a very high price since ancient times.
86	\N	\N	\N	\N	Dinraal's Claw	Dragon Parts	180	\N	{2,22,33,34}	This valuable claw was plucked from the red spirit Dinraal. It was extremely hot before it was removed. You could sell it to a store, but it must have some other use.
87	\N	\N	\N	\N	Dinraal's Scale	Dragon Parts	150	\N	{2,22,33,34}	This precious scale fell from the red spirit Dinraal. It emanates a great heat, You can use it in cooking, but eating it seems like a waste.
88	\N	\N	\N	\N	Electric Keese Wing	Monster Parts	6	\N	{2,22,33,34}	A rare Electric Keese wing. The part of the Electric Keese that produces electricity is not in its wings, so it won't shock you. Toss it in with some critters to make elixirs.
89	\N	\N	\N	\N	Farosh's Claw	Dragon Parts	180	\N	{2,22,33,34}	This claw was plucked from the golden spirit Farosh. Its electricity was expelled before it was removed, so it's safe to handle. A store will buy it, but it must have some other use.
90	\N	\N	\N	\N	Farosh's Scale	Dragon Parts	150	\N	{2,22,33,34}	This precious scale fell from the golden spirit Farosh. It contains powerful electricity within. You can cook it, but eating it seems like a waste.
166	\N	\N	\N	\N	Seared Gourmet Steak	Dish	\N	\N	\N	\N
167	\N	\N	\N	\N	Seared Prime Steak	Dish	\N	\N	\N	\N
168	\N	\N	\N	\N	Seared Steak	Dish	\N	\N	\N	\N
91	\N	\N	\N	\N	Fire Keese Wing	Monster Parts	6	\N	{2,22,33,34}	A rare Fire Keese wing. There isn't enough Fire Keese attached to it to burn you, but it does have a slight warmth to it. It can be used as an ingredient for elixirs.
92	\N	\N	\N	\N	Flint	Minerals	5	\N	{21,24}	Strike it with a metallic weapon to generate a spark. The portable fire starter breaks after one use, but it can create a long-lasting flame if you use it near firewood.
93	\N	\N	\N	\N	Fresh Milk	Food	3	0.5	\N	This fresh milk comes from Hateno cows and white goats kept in the village. It's delicious on its own but can also be used as an ingredient in soups and stews.
94	\N	\N	\N	\N	Giant Ancient Core	Monster Parts	200	\N	{2,22,33,34}	A giant energy crystal made using lost ancient technology. Cores this large are an extremely rare find. A researcher would probably know how to use this.
95	\N	\N	\N	\N	Goat Butter	Food	3	\N	{25}	Butter made from the milk of a domesticated white goat. In addition to being used in dishes like stews and meunière. It's often used when making cakes and other sweets.
96	\N	\N	\N	\N	Goron Spice	Food	4	\N	{25}	Made from several types of spices, this secret Goron seasoning has been handed down for generations. An initial wave of spiciness paves the way for the sweetness.
97	\N	\N	\N	\N	Hinox Guts	Monster Parts	80	\N	{2,22,33,34}	A giant, smelly Hinox organ of unknown function. Upon closer inspection, it appears to be quivering, It can be used to make an elixir, but some say it has other uses, as well.
98	\N	\N	\N	\N	Hinox Toenail	Monster Parts	20	\N	{22,33,34}	A nail obtained from a Hinox. It's as thick as a plate of armor and can be stewed with critters to make elixirs.
99	\N	\N	\N	\N	Hinox Tooth	Monster Parts	35	\N	{22,33,34}	A tooth obtained from a Hinox. It's so large, it's hard to believe it's a real tooth. It can be used to make elixirs.
100	\N	\N	\N	\N	Hylian Rice	Plants	3	1	\N	This grain is a favorite among residents of Kakariko Village. It's grown in regions with a lot of water and is quite versatile. Used as an ingredient in things like risotto and rice balls.
101	\N	\N	\N	\N	Ice Keese Wing	Monster Parts	6	\N	{2,22,33,34}	A rare Ice Keese wing. Its frozen surface gleams attractively, but its usefulness isn't readily apparent.
102	\N	\N	\N	\N	Icy Lizalfos Tail	Monster Parts	35	\N	{2,22,33,34}	The severed tail of an Ice-Breath Lizalfos. Its hard scales and flesh make it unsuitable for cooking, but it's perfect for making elixirs.
103	\N	\N	\N	\N	Keese Eyeball	Monster Parts	20	\N	{2,22,33,34}	A rare material dropped by a defeated Keese. It's fun to look at, but it doesn't seem to have much use at first glance. But it must be good for something...
104	\N	\N	\N	\N	Keese Wing	Monster Parts	2	\N	{2,22,33,34}	The wing of a Keese. It's covered with very short, sharp fur. It's not much use by itself, but you can mix it with critters to make something useful.
105	\N	\N	\N	\N	Lizalfos Horn	Monster Parts	10	\N	{2,22,33,34}	This winding horn once grew atop the head of a Lizalfos. It's too hard to use for cooking, but stores are willing to buy it from you. It can also be used to make elixirs.
106	\N	\N	\N	\N	Lizalfos Talon	Monster Parts	15	\N	{2,22,33,34}	This talon once grew from the elbow of a Lizalfos. You can stew it with critters to make elixirs, but it may have some other uses, as well.
107	\N	\N	\N	\N	Lizalfos Tail	Monster Parts	28	\N	{2,22,33,34}	The severed tail of a Lizalfos. It continues to wriggle even after being separated from its body. That kind of vitality makes it highly valued as an ingredient in elixirs.
108	\N	\N	\N	\N	Luminous Stone	Minerals	70	\N	{1,2,34,35}	This mysterious mineral gives off a pale blue glow in the dark, which some believe to be souls of the undead. Apparently, this stone can be used as a base to make special clothing.
109	\N	\N	\N	\N	Lynel Guts	Monster Parts	200	\N	{2,22,33,34}	This highly sought-after ingredient can only be obtained from a Lynel. It pulses with the vitality of a Lynel, a strength that makes it invaluable as an ingredient for elixirs.
110	\N	\N	\N	\N	Lynel Hoof	Monster Parts	50	\N	{2,22,33,34}	This rare ingredient can be obtained only from a Lynel's foot. It's larger and heavier than a horse's hoof. It's useful for making elixirs but may have uses beyond that.
111	\N	\N	\N	\N	Lynel Horn	Monster Parts	40	\N	{2,22,33,34}	The Lynel this horn once grew upon surely misses it. It's exceptionally hard—so hard that most blades cannot scratch it. Throw it into a stew with critters to make elixirs.
112	\N	\N	\N	\N	Moblin Fang	Monster Parts	12	\N	{2,22,33,34}	A sharp fang obtained from a Moblin. It's too hard to be reshaped into a tool, but it can be tossed into a stew with some critters to create elixirs.
113	\N	\N	\N	\N	Moblin Guts	Monster Parts	25	\N	{2,22,33,34}	This prized ingredient can be obtained from Moblins. Its odor is too pungent for normal food preparation, but stew it with some critters to make elixirs.
114	\N	\N	\N	\N	Moblin Horn	Monster Parts	5	\N	{2,22,33,34}	This splendid horn once grew atop the head of a Moblin. It can't be used in normal food recipes, but it does have some use as an ingredient in making elixirs.
115	\N	\N	\N	\N	Molduga Fin	Monster Parts	30	\N	{22,33,34}	A sturdy fin obtained from a Molduga. It can be used to make elixirs, but there may be someone willing to trade for it.
116	\N	\N	\N	\N	Molduga Guts	Monster Parts	110	\N	{2,22,33,34}	A giant organ obtained from a Molduga. This invaluable ingredient is hard to come by, and its use is shrouded in mystery.
117	\N	\N	\N	\N	Monster Extract	Food	3	\N	{31}	A result of Kilton's research into monsters, this suspicious spice can be used to punch up dishes while cooking. Apparently it can be used to make a number of monstrous meals.
118	\N	\N	\N	\N	Naydra's Claw	Dragon Parts	180	\N	{2,22,33,34}	This valuable claw was plucked from the blue spirit Naydra. It was freezing cold before it was removed. You could sell it, but there must be some other use for it.
119	\N	\N	\N	\N	Naydra's Scale	Dragon Parts	150	\N	{2,22,33,34}	This precious scale fell from the blue spirit Naydra. It contains a bitter cold within. You can use it in cooking, but just eating it seems like a waste.
120	\N	\N	\N	\N	Octo Balloon	Monster Parts	5	\N	{22,33,34}	This inflatable Octorok organ has a lot of lift, so attach it to items you want to see float. Hold it in your hands, and then place it on an object to attach it.
169	\N	\N	\N	\N	Sneaky River Escargot	Dish	\N	\N	\N	\N
121	\N	\N	\N	\N	Octorok Eyeball	Monster Parts	25	\N	{22,33,34}	This can only be obtained from an Octorok-type enemy. You can sell it to a store, or you can make elixirs with it, but it may have other uses, as well.
122	\N	\N	\N	\N	Octorok Tentacle	Monster Parts	10	\N	{22,33,34}	This can only be obtained from an Octorok-type enemy. It's too acidic for cooking, but it's highly valued as an ingredient for elixirs.
123	\N	\N	\N	\N	Opal	Minerals	60	\N	{1,2,34}	A valuable ore that gives off a mesmerizing iridescence similar to the inside of a seashell. It contains the power of water.
124	\N	\N	\N	\N	Raw Bird Thigh	Food	15	1.5	\N	A high-quality piece of meat that's hard to come by. You can eat it raw, but cooking it first will recover more hearts.
125	\N	\N	\N	\N	Raw Bird Drumstick	Food	8	2	\N	This meat is tougher and chewier than a standard steak. Tastes better cooked.
126	\N	\N	\N	\N	Raw Gourmet Meat	Food	35	3	\N	This prized cut of meat is usually from a large animal. Any connoisseur would rank this tender, juicy cut of meat gourmet. Expect an exquisite meal when cooking with this.
127	\N	\N	\N	\N	Raw Meat	Food	8	1	\N	Meat obtained from animals in plains and forests. You can eat it raw, but cooking it will make it more delicious and nutritious.
128	\N	\N	\N	\N	Raw Prime Meat	Food	15	1.5	\N	A fresh, high-quality piece of animal meat. This stuff isn't easy to come by, so savor it. Cook it to recover more hearts.
129	\N	\N	\N	\N	Raw Whole Bird	Food	35	3	\N	This prized meat can be obtained from certain birds. It gets full points for flavor, nutrition, and volume. It pairs perfectly with other ingredients or can be enjoyed alone.
130	\N	\N	\N	\N	Red Chuchu Jelly	Monster Parts	10	\N	{2}	A jiggly substance that normally comes from a Fire Chuchu. It constantly gives off heat. If struck, it will explode in a ball of flame.
131	\N	\N	\N	\N	Red Lizalfos Tail	Monster Parts	35	\N	{2,22,33,34}	The severed tail of a Fire-Breath Lizalfos. Its flavor makes the flesh inedible, but toss it into a stew with some critters, and you'll have yourself a nice elixir.
132	\N	\N	\N	\N	Rock Salt	Food	2	\N	{25}	Crystallized salt from the ancient sea commonly used to season meals. Cannot be eaten in this form.
133	\N	\N	\N	\N	Ruby	Minerals	210	\N	{1,2,34}	A precious red gem mined from large ore deposits found throughout Hyrule. Rubies contain the power of fire and have fetched a high price since ancient times.
134	\N	\N	\N	\N	Sapphire	Minerals	260	\N	{1,2,34}	A precious blue gem mined from natural rock formations. Sapphires contain the very essence of ice. They've been known to fetch a high price since ancient times.
135	\N	\N	\N	\N	Shard of Dinraal's Fang	Dragon Parts	250	\N	{2,22,33,34}	This shard fell from the red spirit Dinraal's fang. It's exceptionally hard and therefore impossible to process, but apparently it can be used as a material for something.
136	\N	\N	\N	\N	Shard of Dinraal's Horn	Dragon Parts	300	\N	{2,22,33,34}	This shard chipped and fell off the red spirit Dinraal. Its horn is said to be the crystallized power of fire itself. It sells for a high price, but rumors say there's some other use for it.
137	\N	\N	\N	\N	Shard of Farosh's Fang	Dragon Parts	250	\N	{2,22,33,34}	This shard fell from the golden spirit Farosh's fang. It's so hard, hammers bounce right off it, so it's impossible to process. Yet rumors say it does have some use as a material.
138	\N	\N	\N	\N	Shard of Farosh's Horn	Dragon Parts	300	\N	{2,22,33,34}	This shard fell off the golden spirit Farosh. It's said the horn is the crystallized power of electricity itself. It sells for a lot, but there must be some other use for it.
139	\N	\N	\N	\N	Shard of Naydra's Fang	Dragon Parts	250	\N	{2,22,33,34}	This shard fell from the blue spirit Naydra's fang. It's much harder than any metal, so it's impossible to process. Yet rumors say it can be used as a material for something.
140	\N	\N	\N	\N	Shard of Naydra's Horn	Dragon Parts	300	\N	{2,22,33,34}	This shard chipped and fell off the blue spirit Naydra. It's said the horn is the crystallized power of ice itself. It sells for quite a bit, but rumors say it has some other use.
141	\N	\N	\N	\N	Star Fragment	Other	300	\N	{2}	A mysterious stone fragment that fell from the sky. It looks like it would fetch a good price, but you may be able to use it in certain recipes as well.
142	\N	\N	\N	\N	Stealthfin Trout	Food	10	1	{2,30}	Consuming the bioluminescent compound that makes this fish glow in the dark will increase concentration. Dishes cooked with it will suppress noise when consumed.
143	\N	\N	\N	\N	Tabantha Wheat	Plants	3	1	\N	This grain is cultivated extensively on the Tabantha Plains. It's ground finely with a millstone to be used in cooking. Use it to make things such as stews and breads.
144	\N	\N	\N	\N	Topaz	Minerals	180	\N	{1,2,34}	This precious yellow gem contains the power of electricity. It's been known to fetch a high price since ancient times.
145	\N	\N	\N	\N	White Chuchu Jelly	Monster Parts	10	\N	{2,22,33,34}	A jiggly substance that came from an Ice Chuchu. It's cool to the touch, and squeezing it seems to relieve stress. If struck, it will explode in a cold mist.
146	\N	\N	\N	\N	Wood	Other	2	\N	{9,21}	A portable bundle of wood. You can use this to make a campfire if you have something to light it.
147	\N	\N	\N	\N	Yellow Chuchu Jelly	Monster Parts	10	\N	{2,22,33,34}	A jiggly substance that came from an Electric Chuchu. Electricity pulses through its gelatinous mass. If struck, it will explode in a burst of electric current.
148	\N	\N	\N	\N	Yellow Lizalfos Tail	Monster Parts	35	\N	{2,22,33,34}	The severed tail of an Electric Lizalfos. Its powerful stench makes it unappetizing and unfit for cooking, but it's highly valued as an ingredient for elixirs.
149	\N	\N	\N	\N	Green Rupee	Other	1	\N	\N	\N
150	\N	\N	\N	\N	Blue Rupee	Other	5	\N	\N	\N
151	\N	\N	\N	\N	Red Rupee	Other	20	\N	\N	\N
152	\N	\N	\N	\N	Purple Rupee	Other	50	\N	\N	\N
153	\N	\N	\N	\N	Silver Rupee	Other	200	\N	\N	\N
154	\N	\N	\N	\N	Bokoblin Horn	Monster Parts	3	\N	{22,33,34}	The severed horn of a Bokoblin, a creature often encountered on the plains of Hyrule. The horn isn't edible, but it can be tossed into a stew with some critters to make an elixir.
155	\N	\N	\N	\N	Blackened Crab	Dish	\N	\N	\N	\N
156	\N	\N	\N	\N	Blueshell Escargot	Dish	\N	\N	\N	\N
157	\N	\N	\N	\N	Roasted Bass	Dish	\N	\N	\N	\N
158	\N	\N	\N	\N	Roasted Bird Drumstick	Dish	\N	\N	\N	\N
159	\N	\N	\N	\N	Roasted Bird Thigh	Dish	\N	\N	\N	\N
160	\N	\N	\N	\N	Roasted Carp	Dish	\N	\N	\N	\N
\.


--
-- Data for Name: regions; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.regions (id, name) FROM stdin;
1	Akkala
2	Central Hyrule
3	Dueling Peaks
4	Eldin
5	Faron
6	Gerudo Highlands
7	Gerudo Wasteland
8	Great Hyrule Forest
9	Great Plateau
10	Hateno
11	Hebra
12	Lake
13	Lanayru
14	Ridgeland
15	Tabantha
\.


--
-- Data for Name: shields; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.shields (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, strength, durability, locations, dropped_by, description) FROM stdin;
1	367	371	372	376	Ancient Shield	70	32	\N	\N	This shield was made using ancient Sheikah technology. Its surface glows blue when raised in defense. Enhanced functionality allows it to deflect Guardian beams.
2	370	374	375	379	Boko Shield	3	5	\N	\N	A Bokoblin-made shield created by attaching a handhold to any flat tree bark picked up off the ground. It's pretty shoddy, so don't expect it to last very long.
3	365	369	370	374	Daybreaker	48	60	\N	\N	This shield was cherished by the Gerudo Champion Urbosa. The gold used to make it was handpicked to ensure a design that is both lightweight and very durable.
4	372	376	377	381	Dragon Bone Shield	25	8	\N	\N	This Boko shield is reinforced with fossilized bone. Its defensive capabilities are respectable, but its predictably slipshod craftsmanship spells low disability.
5	353	357	358	362	Emblazoned Shield	3	12	\N	\N	This shield features a traditional design from Necluda. Its combat capabilities aren't much better than the standard wooden shield, but it found popularity for its design.
6	355	359	360	364	Fisherman's Shield	3	10	\N	\N	Often carried by fishermen for its fish design, which represents hope for a great catch. Its light wooden structure makes it convenient to take on a boat.
7	360	364	365	369	Forest Dweller's Shield	30	18	\N	\N	The Koroks made this shield specifically for Hylians. It's made from the finest hard wood of trees that grow only in the Korok forest, so it's sturdier than it looks.
8	363	367	368	372	Gerudo Shield	20	20	\N	\N	The design of this metal shield has changed over time to match the Gerudo's sword-and-shield fighting style. It's favored by soldiers and travelers alike.
9	376	380	381	385	Guardian Shield	18	10	\N	\N	A shield made with ancient Sheikah technology. It can deflect a Guardian Scout's beam.
10	377	381	382	386	Guardian Shield+	30	13	\N	\N	The larger version of a Guardian shield has had its output level boosted. Its defensive capabilities are comparable to those of a metal shield's.
11	378	382	383	387	Guardian Shield++	42	20	\N	\N	The output level of this shield has been boosted to maximum. Its combat capabilities surpass those of metallic shields, and it can deflect Guardian Scout beams.
12	\N	\N	\N	\N	Hero's Shield	65	90	\N	30th Anniversary series Zelda amiibo	A shield said to have been the favorite of a hero who traveled the open seas. It was apparently a family heirloom, passed down through many generations.
13	354	358	359	363	Hunter's Shield	3	10	\N	\N	Favored by hunters for its rabbit design, which is said to bring luck on hunts. It's easy to use, but its durability leaves something to be desired.
14	350	354	355	359	Hylian Shield	90	800	\N	\N	A shield passed down through the Hyrulean royal family, along with the legend of the hero who wielded it. Its defensive capabilities and durability outshine all other shields.
15	362	366	367	371	Kite Shield	14	16	\N	\N	Rito warriors cherish this shield. Its unique shape is designed with mid-battle flight in mind to facilitate aerial combat.
16	358	362	363	367	Knight's Shield	40	23	\N	\N	A shield favored by the knights who served the Hyrulean royal family. Its sturdy metal construction makes it quite durable, but its weight requires decent skill to wield.
17	373	377	378	382	Lizal Shield	15	8	\N	\N	A common shield found among the Lizalfos. It's made of metal, but its sloppy craftsmanship offers poor durability.
18	379	383	384	388	Lynel Shield	30	12	\N	\N	A sturdy shield favored by Lynels for its defensive and offensive capabilities. First and foremost a shield, but the banded edges can deal slashing attacks when deflecting.
19	380	384	385	389	Mighty Lynel Shield	44	15	\N	\N	This Lynel-made shield has been reinforced with armor and even more blades. Stronger in both defense and offense, it can tear through basic armor when deflecting.
20	351	355	356	360	Pot Lid	1	10	\N	\N	The lid of a large soup pot. It smells vaguely of chicken broth... Yum! It can take quite a beating before breaking.
21	364	368	369	373	Radiant Shield	35	26	\N	\N	This extravagant shield is presented to Gerudo warriors who rise up to the rank of captain. Its apparent opulence is rivaled only by its combat capabilities.
22	374	378	379	383	Reinforced Lizal Shield	22	12	\N	\N	This Lizal shield has been strengthened by adding a different type of metal to the mix. The edge is lined with spikes, so handle with care.
23	369	373	374	378	Royal Guard's Shield	70	14	\N	\N	This shield was forged using ancient Sheikah technology. It boasts extremely high stopping power, but its structural weakness made its low durability impractical for combat.
24	359	363	364	368	Royal Shield	55	29	\N	\N	A shield issued to the Hyrulean royal family's immediate guard detail. It boasts a high defense, but these days it's more a collector's item due to its ornamentation.
25	368	372	373	377	Rusty Shield	3	16	\N	\N	It's likely this rusty old shield once belonged to a knight. It still has some defensive capabilities, but its usefulness has been worn down by time.
26	381	385	386	390	Savage Lynel's Shield	62	20	\N	\N	This ultimate Lynel shield is used only by the white-haired Lynels. It excels at defending against even the most brutal of attacks and cutting down powerful fores when deflecting.
27	366	370	371	375	Shield of the Mind's Eye	16	16	\N	\N	A small Sheikah-made shield. Its design is intended to decrease blind spots without sacrificing too much defense.
28	361	365	366	370	Silver Shield	18	20	\N	\N	A Zora-made shield adorned with intricate ornamentation. It's said that true masters of this shield can redirect attacks as a rock redirects running water.
29	357	361	362	366	Soldier's Shield	16	16	\N	\N	A shield once used by the guards of Hyrule Castle. It's easy to handle, but its core is made of wood, so it can catch fire.
30	371	375	376	380	Spiked Boko Shield	10	7	\N	\N	A Boko shield made of slightly stronger wood. It's been reinforced with animal bones.
31	375	379	380	384	Steel Lizal Shield	35	15	\N	\N	This Lizal shield is adorned with several metal shells as a means of reinforcement. Its defensive capabilities are high, but its weight requires a skilled soldier to bear.
32	356	360	361	365	Traveler's Shield	4	12	\N	\N	A sturdy shield loved by many an adventurer. It is made of animal hide and sturdy wood and is best suited to defending against weak monsters or animals.
33	352	356	357	361	Wooden Shield	2	12	\N	\N	This lightweight, simple shield is ideal for less-experienced fighters. It can withstand light attacks, but blocking stronger blows is not recommended.
\.


--
-- Data for Name: subregions; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.subregions (id, name, region_fk) FROM stdin;
1	Akkala Highlands	1
2	Akkala Sea	1
3	Deep Akkala	1
4	Tarrey Town	1
5	Hyrule Field	2
6	Hyrule Castle	2
7	Dueling Peaks	3
8	Kakariko Village	3
9	Eldin Canyon	4
10	Death Mountain	4
11	Eldin Mountains	4
12	Goron City	4
13	Faron	5
14	Lurelin Village	5
15	Necluda Sea	5
16	Gerudo Highlands	6
17	Gerudo Desert	7
18	Gerudo Town	7
19	Woodland	8
20	Great Hyrule Forest	8
21	Great Plateau	9
22	Mount Lanayru	10
23	Hateno Village	10
24	Necluda Sea	10
25	Hebra Mountains	11
26	Tabantha Tundra	11
27	Faron Grasslands	12
28	Faron Sea	12
29	Lanayru Great Spring	13
30	Lanayru Sea	13
31	Nlanayru Wetlands	13
32	Zora's Domain	13
33	Hyrule Ridge	14
34	Tabantha Frontier	15
35	Rito Village	15
\.


--
-- Data for Name: treasures; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.treasures (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, description, recoverable_materials) FROM stdin;
1	382	386	387	391	Treasure Chest	Fortunes untold (potentially) await the lucky adventurer who finds one of these. Chests can often be found within shrines or at enemy camps, but there may be some crafty folks who think they're safer underground.	\N
2	383	387	388	392	Ore Deposit	This deposit contains a good deal of ore. Breaking the rock will yield rock salt, flint, and other ore and minerals of varying value.	{92,132,73,85,123,133,134,144}
3	384	388	389	393	Rare Ore Deposit	This deposit contains a good deal of precious ore and the occasional rare mineral, like ruby and sapphire. Break it open to see what it has to offer!	{92,73,85,133,134,144}
4	385	389	390	394	Luminous Stone Deposit	This deposit contains quite a bit of luminous stone. Crack it open to get at the easily processed rocks within.	{92,108}
\.


--
-- Data for Name: weapons; Type: TABLE DATA; Schema: public; Owner: webserver
--

COPY public.weapons (id, compendium_id, compendium_id_dlc_2, compendium_id_master_mode, compendium_id_master_mode_dlc_2, name, hands, weapon_type, attack_power, durability, locations, dropped_by, description) FROM stdin;
1	198	201	203	206	Master Sword	1	sword	30	40	\N	\N	The Legendary sword that seals the darkness. Its blade gleams with a sacred luster that can oppose the Calamity. Only a hero chosen by the sword itself may wield it.
2	198	201	203	206	Master Sword (Amplified)	1	sword	40	40	\N	\N	The Legendary sword that seals the darkness, a blade whose sacred glow can combat the Calamity. Link's conquests in the Trial of the Sword have amplified its splendor.
3	198	201	203	206	Master Sword (Awakened)	1	sword	60	188	\N	\N	The Legendary sword that seals the darkness, a blade whose sacred glow can combat the Calamity. Link's triumphs over the Trial of the Sword has awakened its true splendor.
4	199	202	204	207	Tree Branch	1	club	2	4	\N	\N	Wooden branches such as this are pretty common, but it's surprisingly well-balanced. It doesn't do much damage but can serve as a weapon in a pinch.
5	200	203	205	208	Torch	1	club	2	8	\N	\N	This torch will stay lit once ignited. If you put it away, the flame will be extinguished until you light it again.
6	201	204	206	209	Soup Ladle	1	club	4	5	\N	\N	A kitchen implement often used for serving delicious soups. It was carved from the wood of a sturdy tree, so it actually packs quite the wallop.
7	202	205	207	210	Boomerang	1	boomerang	8	18	\N	\N	This throwing weapon was originally used by the forest-dwelling Koroks. Its unique shape allows it to return after being thrown.
8	203	206	208	211	Spring-Loaded Hammer	2	hammer	1	80	\N	\N	This strange hammer is one of Kilton's specialties. Being struck by it doesn't hurt much, but the fourth swing in a string of attacks will send the victim flying.
9	204	207	209	212	Traveler's Sword	1	sword	5	20	\N	\N	A very common sword often kept by travelers to fend off small beasts. It's fairly durable, but a bit unreliable against monsters.
10	205	208	210	213	Soldier's Broadsword	1	sword	14	25	\N	\N	A sword brandished by the soldiers who once protected Hyrule Castle. Its durable blade is well tuned for slaying monsters.
11	206	209	211	214	Knight's Broadsword	1	sword	26	27	\N	\N	Knights of Hyrule once carried this sword. These days it's the weapon of choice for seasoned adventurers thanks to its ease of use and high attack power.
12	207	210	212	215	Royal Broadsword	1	sword	36	36	\N	\N	The Hyrulean royal family would award this sword to knights who achieved remarkable feats. A sword that balances strength and beauty as elegantly as this one is a rare find.
13	208	211	213	216	Forest Dweller's Sword	1	sword	22	27	\N	\N	Koroks made this sword for Hylians. It's made of wood, so it isn't the best choice for head-on attacks. Its original intent was likely clearing vines to forge paths through forests.
14	209	212	214	217	Zora Sword	1	sword	15	27	\N	\N	The ornamentation that adorns this blade is a traditional Zora design. It's forged from a very durable and rust-proof metal.
15	210	213	215	218	Feathered Edge	1	sword	15	27	\N	\N	Rito craftsmen forged this lightweight, double-edge sword so Rito warriors could soar into battle unhindered by its weight.
16	211	214	216	219	Gerudo Scimitar	1	sword	16	23	\N	\N	This common sword is often carried by Gerudo women for self-defense. Its short, curved blade is easily recognized.
17	212	215	217	220	Moonlight Scimitar	1	sword	25	32	\N	\N	Delicate Gerudo carvings decorate this curved sword. The engraved blade is extremely sharp. Apparently it once served ceremonial purposes in festivals.
18	213	216	218	221	Scimitar of the Seven	1	sword	32	60	\N	\N	A famous sword once beloved by the Gerudo Champion Urbosa. It is said that when Urbosa swung this sword in battle, her movements resembled a beautiful dance.
19	214	217	219	222	Eightfold Blade	1	sword	15	26	\N	\N	A single-edged sword traditional to the Sheikah tribe. Forged using ancient technology it just may be among the sharpest conventional weapons ever made.
20	215	218	220	223	Ancient Short Sword	1	sword	40	54	\N	\N	The blade of this sword was made using an ancient power lost to this modern age. Its blade appears only when drawn, and its cutting power surpasses metal swords.
21	216	219	221	224	Rusty Broadsword	1	sword	6	8	\N	\N	A rusty sword used ages ago by a skilled swordsman. It can do some damage in the right hands but also breaks quickly.
22	217	220	222	225	Royal Guard's Sword	1	sword	48	14	\N	\N	A Sheikah-made replica of the sword that seals the darkness. It was made with ancient technology to oppose the Great Calamity, but its low durability made it inefficient.
23	218	221	223	226	Flameblade	1	sword	24	36	\N	\N	This magical sword was forged in the lava of Death Mountain. It leaves white-hot flames in its wake when the blade glows red.
24	219	222	224	227	Frostblade	1	sword	20	30	\N	\N	A magical sword forged in the frigid mountains of the Hebra region. When the blade glows blue, enemies struck by it will become frozen.
25	220	223	225	228	Thunderblade	1	sword	22	36	\N	\N	A magical sword forged and refined by lightning from the Hyrule Hills. When the blade shines with a golden light, it will electrocute enemies struck by it.
26	221	224	226	229	Boko Club	1	club	4	8	\N	\N	A crude Bokoblin club made to clobber small prey. It's essentially a stick, so its durability is low.
27	222	225	227	230	Spiked Boko Club	1	club	12	14	\N	\N	A reinforced Bokoblin club made to maximize damage. The sharpened bones jabbed into it make it a brutal weapon.
28	223	226	228	231	Dragonbone Boko Club	1	club	24	18	\N	\N	This Bokoblin club has been reinforced with fossilized bones to maximize clobbering potential. Only the brawniest of Bokoblins can manage its immense weight.
29	224	227	229	232	Lizal Boomerang	1	boomerang	14	17	\N	\N	A curved sword favored by the Lizalfos. It can be used to attack directly or can be thrown like a boomerang.
30	225	228	230	233	Lizal Forked Boomerang	1	boomerang	24	23	\N	\N	Blue Lizalfos in particular like this weapon. It has one more blade than the Lizal boomerang to give it additional cutting power, and it still returns when thrown.
31	226	229	231	234	Lizal Tri-Boomerang	1	boomerang	36	27	\N	\N	More blades means more attack power! It can be used as a boomerang, but all those blades makes that a bit more dangerous. Carried by Black Lizalfos seasoned in battle.
32	227	230	232	235	Guardian Sword	1	sword	20	17	\N	\N	A sword often wielded by Guardian Scouts. Its blue energy blade is a product of ancient technology. It's not very durable.
33	228	231	233	236	Guardian Sword++	1	sword	30	26	\N	\N	This Guardian sword has enhanced power over the standard model. Its cutting capabilities are improved, and its durability has seen a slight uptick.
34	229	232	234	237	Guardian Sword++	1	sword	40	32	\N	\N	The abilities of this Guardian sword have been boosted to the maximum, as evidenced by its increase in size. It slices through armor like a hot knife through butter!
35	230	233	235	238	Lynel Sword	1	sword	24	26	\N	\N	This Lynel-made sword was designed with smashing in mind rather than slicing. It's on the heavy side compared to what Hylians are used to, but it's very strong.
36	231	234	236	239	Mighty Lynel Sword	1	sword	36	32	\N	\N	This Lynel-made sword boasts more blades and more attack power. A skilled Lynel can draw this sword simply in passing and still cut a foe in two.
37	232	235	237	240	Savage Lynel Sword	1	sword	58	41	\N	\N	A brutal sword carried by white-haired Lynels. The savage blades are strong enough to cut down any foe, no matter how strong.
38	233	236	238	241	Fire Rod	1	rod	5	14	\N	\N	A magical rod that can cast fireballs. The rod will break if it strikes something directly, so use it wisely.
39	234	237	239	242	Meteor Rod	1	rod	10	32	\N	\N	A magical rod that can cast three fireballs at once, crafted by an ancient magician. It will break upon running out of magical energy, so make it last!
40	235	238	240	243	Ice Rod	1	rod	5	14	\N	\N	A magical rod crafted from refined ice found in the Hebra Mountains. This rod can cast waves of freezing air. Great for magic—not so great for melee.
41	236	239	241	244	Blizzard Rod	1	rod	10	32	\N	\N	A magical rod that can cast extreme cold in a wide range. These are crafted from refined ice found at the summit of Hebra Peak. It will break when depleted.
42	236	239	241	244	Boat Oar	2	polearm	14	8	\N	\N	Made for paddling boats, but it was made sturdy enough to fight strong currents. Maybe it's useful for self-defense in a pinch.
43	237	240	242	245	Lightning Rod	2	rod	5	14	\N	\N	A magical rod that can shoot balls of electricity. Its gem contains lightning from the Hyrule Hills. It's not recommended to use as a melee weapon.
44	238	241	243	246	Thunderstorm Rod	1	rod	10	32	\N	\N	A magical rod that can hurl three balls of electricity at once. Its gem contains electricity from the Hyrule Hills, and the rod will break when that electricity runs out.
45	239	242	244	247	Vicious Sickle	1	sickle	16	14	\N	\N	A grim weapon favored by the Yiga. The half-moon shape of the blade allows for the rapid delivery of fatal wounds and serves as a symbol of their terror. Its durability is low.
46	240	243	245	248	Demon Carver	1	carver	40	25	\N	\N	This lethal weapon is forged by the Yiga. Its unique shape facilitates the sound dispatching of any target and strikes fear into the hearts of all who see it.
47	241	245	246	250	Bokoblin Arm	1	arm	5	5	\N	\N	A skeletal arm that keeps moving even after it's severed from its body. It's kind of gross to strap it to your back, but it'll do in a pinch. It's old and fragile, so it's quick to break.
48	242	246	247	251	Lizalfos Arm	1	arm	12	8	\N	\N	The arm of a Stalizalfos that continues to struggle even in death. It can be used as a weapon, but it's very brittle. You can feel it wiggling when you strap it to your back...
49	243	247	248	252	Korok Leaf	2	leaf	1	25	\N	\N	A single swing of this giant, sturdy leaf can create a gust of wind strong enough to blow away light objects.
50	244	248	249	253	Farming Hoe	2	polearm	16	6	\N	\N	A farming tool primarily used for tilling fields. Its fine craftsmanship is sturdy enough to withstand backbreaking fieldwork, but its battle applications are untested.
51	246	250	251	255	Woodcutter's Axe	2	axe	3	47	\N	\N	A woodcutter's tool of choice for felling trees. Its formidable weight and uneven balancing make it a slow, inefficient weapon.
52	247	251	252	256	Double Axe	2	axe	18	52	\N	\N	This double-sided axe was designed with fighting in mind. It's a bit unwieldy, so it requires a well-practiced technique to use efficiently.
53	248	252	253	257	Iron Sledgehammer	2	hammer	12	40	\N	\N	This large iron sledgehammer was originally used for mining, but it works reasonably well as a weapon too.
54	249	253	254	258	Giant Boomerang	2	boomerang	25	40	\N	\N	This massive boomerang requires two hands. Originally used for hunting, it was modified for use as a weapon. The blades on the inner curves make it a bit tricky to wield.
55	250	254	255	259	Traveler's Claymore	2	sword	10	20	\N	\N	A basic two-handed sword often wielded by aspiring adventurers. Its immense weight can knock enemies' shields right out of their hands.
56	251	255	256	260	Soldier's Claymore	2	sword	20	25	\N	\N	A two-handed sword designed for combat. It's heavy and hard to use but has decent build quality and durability.
57	252	256	257	261	Knight's Claymore	2	sword	38	30	\N	\N	Only the most confident of Hyrule Castle's knights carried this two-handed sword. Its cutting edge is finely honed.
58	253	257	258	262	Royal Claymore	2	sword	52	40	\N	\N	A two-handed sword issued to the Hyrulean royal family's immediate guard detail. Its powerful strikes are said to crush an opponent's body and resolve alike.
59	254	258	259	263	Silver Longsword	2	sword	22	30	\N	\N	Although the Zora prefer spears to swords, they made this two-handed weapon using a special metal. It found popularity among Hylians for its unique design.
60	255	259	260	264	Cobble Crusher	2	sword	15	30	\N	\N	A Goron-made two-handed weapon. It's made from thick, hard metal and has no cutting edge, so it relies on its sheer weight to crush all opponents.
61	256	260	261	265	Stone Smasher	2	sword	42	40	\N	\N	A two-handed weapon forged from a rare metal mined in Goron City. Its center of gravity is at its tip, so it uses centrifugal force and its sheer weight to smash opponents flat.
62	257	261	262	266	Boulder Breaker	2	sword	60	60	\N	\N	This two-handed weapon was once wielded by the Goron Champion Daruk. Daruk made swinging it around look easy, but a Hylian would need an immense amount of strength.
63	258	262	263	267	Golden Claymore	2	sword	28	30	\N	\N	Only the most talented Gerudo sword fighters carry this two-handed sword. It's actually much lighter than it appears and is surprisingly easy to wield.
64	259	263	264	268	Eightfold Longblade	2	sword	32	25	\N	\N	A single-edged sword seldom seen in Hyrule. This weapon is passed down through the Sheikah tribe and has an astonishingly sharp edge ideal for slicing.
65	260	264	265	269	Edge of Duality	2	sword	50	35	\N	\N	A curious double-edged sword crafted using Sheikah technology. It was originally made for Hyrulean knights unfamiliar with single-edged blades.
66	261	265	266	270	Ancient Bladesaw	2	sword	55	50	\N	\N	This two-handed sword was forged using ancient Sheikah technology. Its unique rotating blades give it impressive cutting power that will slice enemies to shreds.
67	262	266	267	271	Rusty Claymore	2	sword	12	10	\N	\N	A two-handed sword not properly cared for. Although it can be used as a weapon, its durability is very low. Don't expect it to last for more than a few strikes.
68	263	267	268	272	Royal Guard's Claymore	2	sword	72	15	\N	\N	The Sheikah used the very essence of ancient technology to forge this greatsword. It was designed to oppose the Calamity, but its low durability made it impractical in battle.
69	264	268	269	273	Great Flameblade	2	sword	34	50	\N	\N	This magic-infused greatsword was forged in the fires of Death Mountain by Goron smiths in an ancient age. Attack when the blade glows red to expel flames.
70	265	269	270	274	Great Frostblade	2	sword	30	40	\N	\N	This magic-infused greatsword was forged by smelting ore found in the Hebra Mountains' permafrost. Attack when the blade glows blue to expel freezing air.
71	266	270	271	275	Great Thunderblade	2	sword	32	50	\N	\N	This magic-infused greatsword was forged by the Hyrulean royal family using lightning from the Hyrule Hills. Attack when the blade glows golden to expel lightning.
72	267	271	272	276	Boko Bat	2	club	6	12	\N	\N	A clunky club made by a Bokoblin. If you swing it at an enemy's shield, it may be able to knock the shield out of your foe's hand.
73	268	272	273	277	Spiked Boko Bat	2	bat	18	12	\N	\N	After much consideration by Bokoblins on how to improve the Boko bat, they simply attached sharp spikes to it. A skilled fighter can cause immense damage with this.
74	269	273	274	278	Dragonbone Boko Bat	2	bat	36	16	\N	\N	Used by only the toughest Bokoblin warriors, this Boko bat has been fortified by fossilized bone. It boasts a high durability and is strong enough to beat down powerful foes.
75	270	274	275	279	Moblin Club	2	club	9	12	\N	\N	A crudely fashioned club favored by Moblins. It's carved from a sturdy tree but is sloppily made, so it breaks easily.
76	271	275	276	280	Spiked Moblin Club	2	club	27	18	\N	\N	Animal bone has been affixed to this Moblin club to greatly improve its damage.
77	272	276	277	281	Dragonbone Moblin Club	2	club	45	24	\N	\N	The bone of an ancient beast has been affixed to this Moblin club, further increasing its damage. Moblins carrying these in battle are particularly dangerous.
78	273	277	278	282	Ancient Battle Axe	2	axe	30	15	\N	\N	A weapon used by Guardian Scouts. Its unique blade was forged using ancient technology. Although powerful, its unusual shape causes it to break easily.
79	274	278	279	283	Ancient Battle Axe+	2	axe	45	20	\N	\N	This ancient battle axe's damage output has been increased to maximum. It's sharp enough to cut through almost anything, so it may have been used to forge new routes.
80	275	279	280	284	Ancient Battle Axe++	2	axe	60	25	\N	\N	This ancient battle axe's damage output is scaled up to peak performance. Ancient technology makes it possible to enhance cutting power beyond metal weapons' limits.
81	276	280	281	285	Lynel Crusher	2	crusher	36	20	\N	\N	This two-handed weapon is favored by the Lynels. It may be more accurate to call it a lump of metal than a weapon, but if wielded by a Lynel, it can pound you into a fine dust.
82	277	281	282	286	Mighty Lynel Crusher	2	crusher	54	25	\N	\N	This Lynel-made two-handed weapon has been reinforced to increase its durability and striking power. Its overwhelming heft will crush your foe, shield and all.
83	278	282	283	287	Savage Lynel Crusher	2	crusher	78	35	\N	\N	This Lynel-made two-handed weapon is immensely heavy thanks to a rare metal from Death Mountain's peak. The power of its downward swing is in a class all its own.
84	279	283	284	288	Windcleaver	2	sword	40	25	\N	\N	This sword is favored by high-ranking members of the Yiga. When wielded by a proficient fighter, its unique shape cleaves the very wind and creates a vacuum.
85	280	284	285	289	Moblin Arm	2	arm	15	5	\N	\N	A Moblin bone that continues to move even after being detached from its body. The bone is thick enough to be used as a weapon but is extremely brittle and easily broken.
86	281	285	286	290	Wooden Mop	2	spear	5	8	\N	\N	Just a mop to the untrained eye, it excels at tidying up the place. But it owes its sturdy construction to a true craftsman, so it actually has some combat merit.
87	282	286	287	291	Farmer's Pitchfork	2	spear	2	12	\N	\N	A farming tool used to collect hay efficiently. It's light enough to be used by anyone. The four prongs are very sharp.
88	283	287	288	292	Fishing Harpoon	2	spear	8	12	\N	\N	A fisherman's tool that excels at catching large fish. Its particularly sharp spearhead makes it valuable as a weapon as well.
89	284	288	289	293	Throwing Spear	2	spear	6	20	\N	\N	A specialized spear weighted to excel as a throwing weapon. It's perfectly balanced to be thrown farther than your average spear, able to pierce targets from a great distance.
90	285	289	290	294	Traveler's Spear	2	spear	3	30	\N	\N	A spear used mainly by travelers to fend off wolves and other beasts. It's easy to hold and simple to use.
91	286	290	291	295	Soldier's Spear	2	spear	7	35	\N	\N	A long spear once used by the guards of Hyrule Castle. Easy to use but difficult to master. The iron tip is very sturdy and the shaft will not burn when exposed to flame.
92	287	291	292	296	Knight's Halberd	2	spear	13	40	\N	\N	A spear used by knights adept in mounted combat. The spearhead is modeled after an axe.
93	288	292	293	297	Royal Halberd	2	spear	26	50	\N	\N	This spear was issued to the knights who guarded Hyrule Castle's throne room. Its ornate design was applied by a craftsman in service to the royal family.
94	289	293	294	298	Forest Dweller's Spear	2	spear	11	35	\N	\N	The Koroks made this spear for Hylians. The shaft is made from a light, sturdy wood, offering ease of use. The spearhead is made from a much harder wood, offering strength.
95	290	294	295	299	Zora Spear	2	spear	9	40	\N	\N	This spear is a Zora's weapon of choice. It's lighter than it looks due to being made from a special metal and is used by the Zora for both fishing and protecting their domain.
96	291	295	296	300	Silverscale Spear	2	spear	12	40	\N	\N	The most skilled Zora fighters wield this spear. Its beautiful fish-tail design belies its impressive strength; the spearhead can pierce even the toughest scales.
97	292	296	297	301	Ceremonial Trident	2	spear	14	40	\N	\N	A spear modeled after the Lightscale Trident wielded by the Zora Champion Mipha. They may be identical in appearance, but this spear's strength and durability are inferior.
98	293	297	298	302	Lightscale Trident	2	spear	22	70	\N	\N	A spear of peerless grace cherished by the Zora Champion Mipha. Although Mipha specialized in healing abilities, her spearmanship was in a class all its own.
99	294	298	299	303	Drillshaft	2	spear	14	50	\N	\N	Goron artisans used recycled metal to forge this weapon. The tip is made from an old excavation bore, which affords it unmatched piercing capabilities.
100	295	299	300	304	Feathered Spear	2	spear	10	35	\N	\N	Its lightweight design is a hallmark of Rito craftsmanship. It's made from light and sturdy materials, which afford Rito warriors ease of use during aerial combat.
101	296	300	301	305	Gerudo Spear	2	spear	16	35	\N	\N	This spear's center of gravity is in its tip, making it a bit unwieldy for the average fighter. But in the hands of a skilled Gerudo warrior, it's a weapon of reliable strength.
102	297	301	302	306	Serpentine Spear	2	spear	12	35	\N	\N	The spearhead of this weapon is uniquely Sheikah in design. Spear masters of the Sheikah tribe can use the crescent shape to snag their opponents and deliver brutal cuts.
103	298	302	303	307	Ancient Spear	2	spear	30	50	\N	\N	This spear is the result of countless hours of research into the ancient technology used by Guardians. The glowing spearhead has high piercing potential.
104	299	303	304	308	Rusty Halberd	2	spear	5	15	\N	\N	A rusty polearm likely used by knights from an age past. The spearhead is in bad shape due to prolonged exposure to the elements, so its durability is low.
105	300	304	305	309	Royal Guard's Spear	2	spear	32	15	\N	\N	This Sheikah-made spear was created using ancient technology to combat the Calamity. Its attack power is very high, but a critical design flaw left it with poor durability.
106	301	305	306	310	Flamespear	2	spear	24	50	\N	\N	A magical spear forged in the magma of Death Mountain. Attack when the blade glows to expel powerful flames.
107	302	306	307	311	Frostspear	2	spear	20	40	\N	\N	A magical spear forged from ancient ice taken from the Hebra Mountains. Attack when the blade glows blue to chill the air and freeze your foe.
108	303	307	308	312	Thunderspear	2	spear	22	50	\N	\N	A magical spear that contains thunder from Thundra Plateau in its tip. Attack when the blade glows with a golden light to unleash an electrical attack.
109	304	308	309	313	Boko Spear	2	spear	2	12	\N	\N	A spear recklessly carved from a tree branch. Useful for skewering fish and meat, which can then be cooked over a flame. If it's close to breaking, consider throwing it.
110	305	309	310	314	Spiked Boko Spear	2	spear	6	15	\N	\N	A Boko spear enhanced with sharpened animal bones. It's light, easy to use, and deals a decent amount of damage.
111	306	310	311	315	Dragonbone Boko Spear	2	spear	12	20	\N	\N	This Boko spear has been strengthened with fossilized bones. The bones are positioned outward so the fangs bite at the opponent. Beware Bokoblins carrying this weapon.
112	307	311	312	316	Moblin Spear	2	spear	4	15	\N	\N	This wooden spear is most often used by Moblins. It's made from a hastily whittled tree, so its stabbing power and durability are both pretty low.
113	308	312	313	317	Spiked Moblin Spear	2	spear	9	20	\N	\N	This Moblin-made spear uses a horned animal bone as the spearhead. Like many Moblin weapons it's sloppily made, but this one packs some respectable piercing power.
114	309	313	314	318	Dragonbone Moblin Spear	2	spear	15	25	\N	\N	This spear is a fan favorite among Moblins. The spearhead is made from fossilized bones adorned with spikes, which greatly increases its stabbing power.
115	310	314	315	319	Lizal Spear	2	spear	7	18	\N	\N	Judging by the harpoon-like spearhead of this Lizalfos-made spear, the Lizalfos use it for fishing as well as combat. Try not to get caught on the wrong end of its barbs.
116	311	315	316	320	Enhanced Lizal Spear	2	spear	12	22	\N	\N	Judging by the harpoon-like spearhead of this Lizalfos-made spear, the Lizalfos use it for fishing as well as combat. Try not to get caught on the wrong end of its barbs.
117	312	316	317	321	Forked Lizal Spear	2	spear	18	28	\N	\N	Skilled Lizalfos warriors tend to favor this spear. What it lacks in piercing power, it makes up for with the brutal wounds its dual ripping blades will inflict.
118	313	317	318	322	Guardian Spear	2	spear	10	20	\N	\N	Wielded by Guardian Scouts, this spear has a high piercing power and is a testament to the Sheikah's high level of technology. the spearhead appears only when brandished.
119	314	318	319	323	Guardian Spear+	2	spear	15	25	\N	\N	The tip of this Guardian spear has been enlarged and strengthened. It's a bit shorter than your average spear, perhaps to facilitate use in tight spaces.
120	315	319	320	324	Guardian Spear++	2	spear	20	35	\N	\N	This Guardian spear's output has been boosted to the maximum. The spearhead is designed for optimal stabbing, capable of easily piercing most armor.
121	316	320	321	325	Lynel Spear	2	spear	14	25	\N	\N	The crescent-shaped spearhead of this Lynel-made weapon gives it poor balance, making it difficult to wield. Lynels, however, can swing it effectively with a single hand.
122	317	321	322	326	Mighty Lynel Spear	2	spear	20	35	\N	\N	The weight and cutting edge of this Lynel-made spear have both been enhanced. It's immensely heavy for a Hylian, but a Lynel can cleave through rock with a single swing.
123	318	322	323	327	Savage Lynel Spear	2	spear	30	45	\N	\N	White-haired Lynels favor this brutal spear. Its axe-like spearhead and exceptional weight give it absolute destructive power.
124	\N	244	\N	249	One-Hit Obliterator	1	sword	\N	\N	\N	\N	A weapon that defeats foes with one hit (Except for Talus), and causes the user to die in one hit. It loses its sheen and power after two consecutive uses, but will eventually regain both.
125	\N	\N	\N	\N	Goddess Sword	1	sword	28	45	\N	\N	A sword said to have once belonged to a hero from the sky. Its blade houses the fire of the Goddess. When wielded, a strange yet heavenly breeze kicks up around you.
126	\N	\N	\N	\N	Sea-Breeze Boomerang	1	boomerang	20	20	\N	\N	A boomerang said to have been used by a hero who traveled the Great Sea. It smells faintly of salt water.
127	\N	\N	\N	\N	Sword	1	sword	22	27	\N	\N	A sword once wielded by a hero in an ancient age. When grasped, a strange sense of nostalgia washes over you. Take it when going alone would otherwise be dangerous.
128	\N	\N	\N	\N	Biggoron's Sword	2	sword	50	60	\N	\N	A legendary greatsword forged by a Goron craftsman for a hero who traveled through time. The exceptionally sharp cutting edge is a testament to the craftsman's mastery.
129	\N	\N	\N	\N	Fierce Deity Sword	2	sword	60	35	\N	\N	A peculiar greatsword allegedly used by a hero from a world in which the moon threatened to fall. It slashes wildly in battle as if possessed by a fierce deity.
130	\N	\N	\N	\N	Sword of the Six Sages	2	sword	48	50	\N	\N	The Six Sages are said to have forged this longsword to seal a demon king in the world where the hero fought against the beasts of twilight. The blade shines with a holy luster.
\.


--
-- Name: creatures creatures_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.creatures
    ADD CONSTRAINT creatures_pkey PRIMARY KEY (id);


--
-- Name: creature_views; Type: MATERIALIZED VIEW; Schema: public; Owner: webserver
--

CREATE MATERIALIZED VIEW public.creature_views AS
 SELECT creatures.compendium_id,
    creatures.compendium_id_dlc_2,
    creatures.compendium_id_master_mode,
    creatures.compendium_id_master_mode_dlc_2,
    creatures.name,
    creatures.creature_type,
    array_agg(recoverable_materials.name) AS recoverable_materials,
    creatures.description
   FROM (public.creatures
     LEFT JOIN public.recoverable_materials ON ((recoverable_materials.id = ANY (creatures.recoverable_materials))))
  GROUP BY creatures.id
  ORDER BY creatures.id
  WITH NO DATA;


ALTER TABLE public.creature_views OWNER TO webserver;

--
-- Name: materials materials_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.materials
    ADD CONSTRAINT materials_pkey PRIMARY KEY (id);


--
-- Name: material_views; Type: MATERIALIZED VIEW; Schema: public; Owner: webserver
--

CREATE MATERIALIZED VIEW public.material_views AS
 SELECT materials.compendium_id,
    materials.compendium_id_dlc_2,
    materials.compendium_id_master_mode,
    materials.compendium_id_master_mode_dlc_2,
    materials.name,
    materials.material_type,
    materials.value,
    materials.restores,
    materials.description,
    array_agg(materials_additional_uses.additional_use) AS additional_uses
   FROM (public.materials
     LEFT JOIN public.materials_additional_uses ON ((materials_additional_uses.id = ANY (materials.additional_uses))))
  GROUP BY materials.id
  ORDER BY materials.id
  WITH NO DATA;


ALTER TABLE public.material_views OWNER TO webserver;

--
-- Name: monsters monsters_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.monsters
    ADD CONSTRAINT monsters_pkey PRIMARY KEY (id);


--
-- Name: monster_views; Type: MATERIALIZED VIEW; Schema: public; Owner: webserver
--

CREATE MATERIALIZED VIEW public.monster_views AS
 SELECT monsters.compendium_id,
    monsters.compendium_id_dlc_2,
    monsters.compendium_id_master_mode,
    monsters.compendium_id_master_mode_dlc_2,
    monsters.name,
    monsters.monster_type,
    monsters.health,
    monsters.base_damage,
    array_agg(recoverable_materials.name) AS recoverable_materials,
    monsters.description
   FROM (public.monsters
     LEFT JOIN public.recoverable_materials ON ((recoverable_materials.id = ANY (monsters.recoverable_materials))))
  GROUP BY monsters.id
  ORDER BY monsters.id
  WITH NO DATA;


ALTER TABLE public.monster_views OWNER TO webserver;

--
-- Name: treasures treasures_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.treasures
    ADD CONSTRAINT treasures_pkey PRIMARY KEY (id);


--
-- Name: treasure_views; Type: MATERIALIZED VIEW; Schema: public; Owner: webserver
--

CREATE MATERIALIZED VIEW public.treasure_views AS
 SELECT treasures.compendium_id,
    treasures.compendium_id_dlc_2,
    treasures.compendium_id_master_mode,
    treasures.compendium_id_master_mode_dlc_2,
    treasures.name,
    array_agg(recoverable_materials.name) AS recoverable_materials,
    treasures.description
   FROM (public.treasures
     LEFT JOIN public.recoverable_materials ON ((recoverable_materials.id = ANY (treasures.recoverable_materials))))
  GROUP BY treasures.id
  ORDER BY treasures.id
  WITH NO DATA;


ALTER TABLE public.treasure_views OWNER TO webserver;

--
-- Name: arrows arrows_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.arrows
    ADD CONSTRAINT arrows_pkey PRIMARY KEY (id);


--
-- Name: bows bows_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.bows
    ADD CONSTRAINT bows_pkey PRIMARY KEY (id);


--
-- Name: location_types location_types_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.location_types
    ADD CONSTRAINT location_types_pkey PRIMARY KEY (id);


--
-- Name: locations locations_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: materials_additional_uses materials_additional_uses_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.materials_additional_uses
    ADD CONSTRAINT materials_additional_uses_pkey PRIMARY KEY (id);


--
-- Name: recoverable_materials recoverable_materials_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.recoverable_materials
    ADD CONSTRAINT recoverable_materials_pkey PRIMARY KEY (id);


--
-- Name: regions regions_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (id);


--
-- Name: shields shields_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.shields
    ADD CONSTRAINT shields_pkey PRIMARY KEY (id);


--
-- Name: subregions subregions_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.subregions
    ADD CONSTRAINT subregions_pkey PRIMARY KEY (id);


--
-- Name: weapons weapons_pkey; Type: CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.weapons
    ADD CONSTRAINT weapons_pkey PRIMARY KEY (id);


--
-- Name: locations locations_location_type_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_location_type_fk_fkey FOREIGN KEY (location_type_fk) REFERENCES public.location_types(id);


--
-- Name: locations locations_region_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_region_fk_fkey FOREIGN KEY (region_fk) REFERENCES public.regions(id);


--
-- Name: locations locations_subregion_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_subregion_fk_fkey FOREIGN KEY (subregion_fk) REFERENCES public.subregions(id);


--
-- Name: subregions subregions_region_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webserver
--

ALTER TABLE ONLY public.subregions
    ADD CONSTRAINT subregions_region_fk_fkey FOREIGN KEY (region_fk) REFERENCES public.regions(id);


--
-- Name: creature_views; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: webserver
--

REFRESH MATERIALIZED VIEW public.creature_views;


--
-- Name: location_views; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: webserver
--

REFRESH MATERIALIZED VIEW public.location_views;


--
-- Name: material_views; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: webserver
--

REFRESH MATERIALIZED VIEW public.material_views;


--
-- Name: monster_views; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: webserver
--

REFRESH MATERIALIZED VIEW public.monster_views;


--
-- Name: treasure_views; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: webserver
--

REFRESH MATERIALIZED VIEW public.treasure_views;


--
-- PostgreSQL database dump complete
--


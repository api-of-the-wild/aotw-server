#!/bin/bash

# any future command that fails will exit the script
set -e

whoami

# Delete the old repo
sudo rm -rf /home/ubuntu/aotw-server/

# clone the repo again
git clone https://gitlab.com/api-of-the-wild/aotw-server

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
# source /home/ubuntu/.nvm/nvm.sh

# stop the previous pm2
whoami
# pm2 stop all;
# pm2 kill

# starting pm2 daemon
# pm2 status

cd /home/ubuntu/aotw-server

#install npm packages
echo "Install dependencies with yarn..."
yarn install

#Restart the node server
sudo yarn start:prod